import React from 'react'
import PropTypes from 'prop-types'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faSquareCheck, faSquareXmark } from "@fortawesome/free-solid-svg-icons"

function StatusIcon(props) {

	const icon = props.status ? faSquareCheck : faSquareXmark

	const style = {
		color: props.status ? 'green' : 'red',
		fontSize: '2rem'
	}

	return (
		<FontAwesomeIcon icon={icon} style={style} />
	)
}

StatusIcon.propTypes = {
	status: PropTypes.bool
}
StatusIcon.defaultProps = {
	status: true
}

export default StatusIcon
