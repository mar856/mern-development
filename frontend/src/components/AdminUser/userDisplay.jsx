import React, { useEffect, useMemo, useState } from 'react'
import userlist from '../../../../backend/assets/userlist.json'
import adminlist from '../../../../backend/assets/adminlist.json'

import "./userDisplay.css"

function User() {
    // userlist
    const [userArray, setUserArray] = useState([])
    // adminlist
    const [adminArray, setAdminArray] = useState([])

    
    useEffect(() => {
        setUserArray(userlist),
        setAdminArray(adminlist)
    }, [])



    const isAdmin = (targetId) => {
        const admin = adminArray.some(item => item._id.$oid === targetId)
        return admin
    }
    
    const adminStatus = (event,item) => {
        console.log(item)
        console.log(event.target.className)
        const status = event.target.className == "green" ? "Remove" : "Add"
        const confirmation = window.confirm(`Are you sure you want to ${status} ${item.firstname} ${item.lastname} as Admin?`);

        if (confirmation){
            if (status == "Remove"){
                const updated = adminArray.filter(ids => ids._id.$oid !== item._id.$oid);
                setAdminArray(updated)
            } else{
                const newAdmin = {"_id":{"$oid": item._id.$oid}}
                setAdminArray([...adminArray, newAdmin]);
                console.log(adminArray)
            }
        }
    }

    return (
        <>
            <table className='usertable'>
                <thead>
                    <tr>
                        <th>First Name</th> 
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Admin</th>
                    </tr>
                </thead>

                <tbody>
                    {userArray.map((item,index)=> (
                        <tr key={index}>
                            <td>{item.firstname}</td>
                            <td>{item.lastname}</td>
                            <td>{item.email}</td>
                            <td><button onClick={(event)=> adminStatus(event, item)} className={isAdmin(item._id.$oid) ? "green" : "red"}></button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}

User.propTypes = {}

export default User
