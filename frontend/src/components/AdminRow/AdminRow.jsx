import React, { useEffect, useMemo, useState } from 'react'
import PropTypes from 'prop-types';
import "./AdminRow.css"

function AdminRow(props) {
    const old_data = props.old
    const new_data = props.new
    return(
        <>
            <div className='toggle-container'>
                <div className='non-attributes'>
                    <div className='old'>
                        <div>
                            <img src={old_data.image_url} alt="" />
                        </div>
                        <p><strong>Name: </strong>{old_data.name}</p>
                        <p><strong>Subtitle: </strong>{old_data.subtitle}</p>
                        <p><strong>Description: </strong>{old_data.description}</p>
                    </div>
                    <div className='new'>
                        <div  className={old_data.image_url!==new_data.image_url ? 'changed' : null}>
                            <img src={new_data.image_url} alt="" />
                        </div>
                        <p className={old_data.name!==new_data.name ? 'changed' : null}><strong>Name: </strong> {new_data.name}</p>
                        <p className={old_data.subtitle!==new_data.subtitle? 'changed' : null}><strong>Subtitle: </strong>{new_data.subtitle}</p>
                        <p className={old_data.description!==new_data.description ? 'changed' : null}><strong>Description: </strong>{new_data.description}</p>
                    </div>
                </div>
                <div>

                </div>
                <table className='history-attribute-table'>
                    <thead>
                        <tr>
                            <th>Old</th>
                            <th>Attribute</th>
                            <th>New</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{old_data.strength}</td>
                            <td>strength</td>
                            <td className={new_data.strength!==old_data.strength ? 'changed': null}>{new_data.strength}</td>
                        </tr>
                        <tr>
                            <td>{old_data.speed}</td>
                            <td>speed</td>
                            <td className={new_data.speed!==old_data.speed ? 'changed': null}>{new_data.speed}</td>
                        </tr>
                        <tr>
                            <td>{old_data.skill}</td>
                            <td>skill</td>
                            <td className={new_data.skill!==old_data.skill ? 'changed': null}>{new_data.skill}</td>
                        </tr>
                        <tr>
                            <td>{old_data.fear_factor}</td>
                            <td>fear_factor</td>
                            <td className={new_data.fear_factor!==old_data.fear_factor ? 'changed': null}>{new_data.fear_factor}</td>
                        </tr>
                        <tr>
                            <td>{old_data.power}</td>
                            <td>power</td>
                            <td className={new_data.power!==old_data.power? 'changed': null}>{new_data.power}</td>
                        </tr>
                        <tr>
                            <td>{old_data.intelligence}</td>
                            <td>intelligence</td>
                            <td className={new_data.intelligence!==old_data.intelligence? 'changed': null}>{new_data.intelligence}</td>
                        </tr>
                        <tr>
                            <td>{old_data.wealth}</td>
                            <td>wealth</td>
                            <td className={new_data.wealth!==old_data.wealth ? 'changed': null}>{new_data.wealth}</td>
                        </tr>
                    </tbody>
                    
                </table>
            </div>
        </>
        
    )
}

AdminRow.propTypes = {}

export default AdminRow
