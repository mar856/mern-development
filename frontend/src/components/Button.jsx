import PropTypes from 'prop-types';

import ReactButton from 'react-bootstrap/Button';

function Button(props) {

    Button.propTypes = {
        title: PropTypes.any,
        type: PropTypes.oneOf(['submit', 'button', 'reset']),
        link: PropTypes.string,
        size: PropTypes.string,
        variant: PropTypes.string,
        alt: PropTypes.string,
        onClick: PropTypes.func,
        char1: PropTypes.string,
        char2: PropTypes.string
    }
    Button.defaultProps = {
        title: 'button',
        type: 'button',
        link: '',
        size: 'lg',
        variant: 'primary',
        onClick: () => { },
        char1: '',
        char2: ''
    }

    const isString = (data) => {
        return typeof data === 'string';
    }

    return (
        <>
            <ReactButton
                type={props.type}
                href={props.link}
                size={props.size}
                variant={props.variant}
                onClick={props.onClick}
                alt={props.alt}
                {...(props.char1 && props.char2
                    ? { 'data-char1': props.char1, 'data-char2': props.char2 }
                    : {})
                }
            >
                {
                    isString(props.title)
                        ? props.title.toUpperCase()
                        : props.title
                }
            </ReactButton >
        </>
    )
}

export default Button
