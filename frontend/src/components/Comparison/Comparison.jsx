import React from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCircleCheck, faCircleXmark } from "@fortawesome/free-regular-svg-icons"

function Comparison(props) {

    const style = {
        backgroundColor: props.status ? 'green' : 'red',
        padding: '10% 0px',
        fontSize: '2rem'
    }

    const icon = props.status ? faCircleCheck : faCircleXmark

    return (
        <div style={style}>
            <FontAwesomeIcon icon={icon} size='xl' />
        </div>
    )
}

Comparison.propTypes = {
    status: PropTypes.bool
}
Comparison.defaultProps = {
    status: true
}

export default Comparison
