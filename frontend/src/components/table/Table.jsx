import PropTypes from 'prop-types'
import { usePagination, useTable } from 'react-table'
import { Table as BTable, Button, ButtonGroup, Form } from 'react-bootstrap'

function Table(props) {
  const columns = props.columns
  const data = props.data
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable({
    columns,
    data,
    initialState: { pageIndex: 0 }
  },
    usePagination
  )
  return (
    <>
      <BTable {...getTableProps()} hover>
        {
          props.header
            ? (
              <thead>
                {headerGroups.map(headerGroup => (
                  <tr {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map(column => (
                      <th {...column.getHeaderProps()}>
                        {column.render('Header')}
                      </th>
                    ))}
                  </tr>
                ))}
              </thead>
            )
            : ''
        }
        <tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row)
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return <td {...cell.getCellProps()}>
                    {cell.render('Cell')}
                  </td>
                })}
              </tr>
            )
          })}
        </tbody>
      </BTable>
      {
        props.pagination
          ? <div className="d-flex justify-content-between align-items-center">
            <ButtonGroup>
              <Button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
                {'<<'}
              </Button>{' '}
              <Button onClick={() => previousPage()} disabled={!canPreviousPage}>
                {'<'}
              </Button>{' '}
              <Button onClick={() => nextPage()} disabled={!canNextPage}>
                {'>'}
              </Button>{' '}
              <Button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
                {'>>'}
              </Button>{' '}
            </ButtonGroup>
            <span>
              Page{' '}
              <strong>
                {pageIndex + 1} of {pageOptions.length}
              </strong>{' '}
            </span>
            <span>
              Total data{' '}
              <strong>
                {props.data.length}
              </strong>{' '}
            </span>
            <span>
              | Go to page:{' '}
              <Form.Control
                type="number"
                defaultValue={pageIndex + 1}
                min={1}
                max={pageOptions.length}
                onChange={e => {
                  const page = e.target.value ? Number(e.target.value) - 1 : 0
                  gotoPage(page)
                }}
                style={{ width: '50px' }}
              />
            </span>{' '}
            <Form.Select
              className="w-auto"
              value={pageSize}
              onChange={e => {
                setPageSize(Number(e.target.value))
              }}
            >
              {[10, 20, 30, 40, 50].map(pageSize => (
                <option key={pageSize} value={pageSize}>
                  Show {pageSize}
                </option>
              ))}
            </Form.Select>
          </div>
          : ''
      }
    </>
  )
}

Table.defaultProps = {
  data: [],
  columns: [],
  onClick: () => { },
  pagination: true,
  header: true
}

Table.propTypes = {
  onClick: PropTypes.func,
  pagination: PropTypes.bool,
  header: PropTypes.bool
}

export default Table