import { Form, NavDropdown } from 'react-bootstrap'
import PropTypes from 'prop-types'

function Notification(props) {
    const notifications = props.data;

    const listNotification = notifications.map(notification =>
        < NavDropdown.Item href={notification.url} >
            <Form.Text>{notification.message}</Form.Text>
        </NavDropdown.Item >);

    return (
        <>
            {listNotification}
        </>
    )
}

Notification.defaultProps = {
    data: []
}

Notification.propTypes = {
    data: PropTypes.arrayOf(
        PropTypes.shape({
            url: PropTypes.string,
            message: PropTypes.string,
        })
    )
}

export default Notification