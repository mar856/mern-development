import React from 'react'
import PropTypes from 'prop-types'
import { Figure } from 'react-bootstrap'

function Picture({ dimension, height, src }) {

    return (
        <Figure.Image
            width={dimension}
            height={dimension}
            src={src}
            alt={src}
        />
    )
}

Picture.defaultProps = {
    dimension: '38%',
    src: '',
    alt: '',
}

Picture.propTypes = {
    dimension: PropTypes.string,
    src: PropTypes.string,
    alt: PropTypes.string,
}

export default Picture
