import React, { useEffect, useMemo, useState } from 'react'
import "./historyDisplay.css"
import AdminRow from '../AdminRow/AdminRow'

import {faSpinner} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

function History() {
    const history_data = [
        {
            "_id": "1",
            "status": "Approved", 
            "contributor": "Anita Simpson",
            "approvedBy": "Anita Simpson",
            "actionType": "Add",
            "date": "2024-05-05T12:00:00Z",
            "version": {
                "old": {
                    "name": "Batman",
                    "subtitle": "The Dark Knight",
                    "description": "Batman is a fictional superhero appearing in American comic books published by DC Comics. The character was created by artist Bob Kane and writer Bill Finger, and first appeared in Detective Comics #27 in 1939.",
                    "image_url": "https://upload.wikimedia.org/wikipedia/en/thumb/5/56/Batman_Logo.svg/800px-Batman_Logo.svg.png",
                    "strength": 40,
                    "speed": 40,
                    "skill": 90,
                    "fear_factor": 80,
                    "power": 80,
                    "intelligence": 90,
                    "wealth": 100
                },
                "new": {
                    "name": "Batman",
                    "subtitle": "The Dark Knight",
                    "description": "Batman is a phenomenal fictional superhero appearing in American comic books published by DC Comics. The character was created by artist Bob Kane and writer Bill Finger, and first appeared in Detective Comics #27 in 1939.",
                    "image_url": "https://us.123rf.com/450wm/hunmanart/hunmanart2301/hunmanart230100001/205186176-saint-petersburg-russia-january-21-2023-batman-icon-with-white-eyes-isolated-icon-flat-style-vector.jpg?ver=6",
                    "strength": 20,
                    "speed": 40,
                    "skill": 90,
                    "fear_factor": 70,
                    "power": 80,
                    "intelligence": 100,
                    "wealth": 10
                },
            }
        },
    ]
    
    // history
    const [historyArray, setHistoryArray] = useState([])
    
    const [selected, setSelected] = useState(null)

    const toggle = (index) => {
        if (selected == index){
            return setSelected(null)
        }
        setSelected(index)
        console.log(selected)
    }

    useEffect(() => {
        setHistoryArray(history_data)
    }, [])

    const changeStatus = (item) => {
        if (item.status === "Pending"){
            console.log("Hello")
        } else{
            console.log("No")
        }
    }
    
    const changePending = () => {
        const confirmation = window.confirm("Do you confirm the changes???");

        if (confirmation){
            console.log(confirmation)
        }
    }
    
    return (
        <>
            <table className='historytable'>
                <thead>
                    <tr>
                        <th>Changed By</th> 
                        <th>Action</th>
                        <th>Status</th>
                        <th>Approved By</th>
                        <th>Date</th>
                        <th>Details</th>
                    </tr>
                </thead>

                <tbody>
                    {historyArray.map((item,index)=> (
                        <>  
                            <tr key={index}>
                                <td>{item.contributor}</td>
                                <td>{item.actionType}</td>
                                <td>{
                                    <>
                                        <div onClick={() => changeStatus(item)}>
                                        {item.status}
                                        </div>
                                    </>
                                }</td>
                                <td>{item.approvedBy}</td>
                                <td>{item.date}</td>
                                <td><button className='toggle-button' onClick={() => toggle(index)}>{selected === index ? '-' : '+'}</button></td>
                            </tr>
                            <tr>
                                <td className={selected !== index ? "noshow": "show"} colSpan={6}>
                                    <AdminRow 
                                        old={item.version.old}
                                        new={item.version.new}
                                    />
                                </td>
                            </tr>
                        </>
                        
                    ))}
                </tbody>
            </table>
        </>
    )
}

History.propTypes = {}

export default History
