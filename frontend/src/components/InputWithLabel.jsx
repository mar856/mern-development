import Form from 'react-bootstrap/Form'
import PropTypes from 'prop-types';

function InputWithLabel(props) {
    return (
        <>
            <Form.Group className='form-group'>
                <Form.Label htmlFor={props.name}>{props.name.toUpperCase()}</Form.Label>
                <Form.Control
                    type={props.type}
                    id={props.name}
                    name={props.name}
                    //value={props.value}
                    onChange={props.onChange}
                />
            </Form.Group>
        </>
    )
}

InputWithLabel.propTypes = {
    name: PropTypes.string,
    type: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
}

InputWithLabel.defaultProps = {
    name: '',
    type: '',
    value: '',
    onChange: () => { }
}

export default InputWithLabel
