import PropTypes from 'prop-types'
import { Container } from "react-bootstrap"

import RangeSlider from "../../form/range/RangeSlider"

import './FilterBox.css'

function FilterBox(props) {
    return (
        <>
            <Container as="section" id="filters">
                <h3>Filters</h3>
                <RangeSlider
                    attribute={props.characterAttribute}
                    onChange={props.onChange}
                />
            </Container>
        </>
    )
}

FilterBox.defaultProps = {
    characterAttribute: [
        { name: "strength" },
        { name: "speed" },
        { name: "skill" },
        { name: "fear-factor" },
        { name: "power" },
        { name: "intelligence" },
        { name: "wealth" }
    ],
    onchange: () => { }
}

FilterBox.propTypes = {
    characterAttribute: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string
        })
    ),
    onChange: PropTypes.func
}

export default FilterBox