import PropTypes from 'prop-types'
import { Form } from 'react-bootstrap'



import './Search.css'

function Search(props) {
    return (
        <Form>
            <Form.Control
                type="text"
                id='search-input'
                placeholder={'Search ' + props.name}
                onKeyUp={props.onKeyup}
            />
        </Form>
    )
}

Search.defaultProps = {
    name: "character",
    onKeyup: () => { }
}
Search.propTypes = {
    name: PropTypes.string,
    onKeyup: PropTypes.func
}

export default Search
