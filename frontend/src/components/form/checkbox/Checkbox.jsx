import React from 'react'
import PropTypes from 'prop-types'
import { InputGroup } from 'react-bootstrap'

import './Checkbox.css'

function Checkbox({ checked, onChange, value }) {
    return (
        <>
            <InputGroup.Checkbox
                defaultChecked={checked}
                onChange={onChange}
                defaultValue={value}
            />
        </>
    )
}

Checkbox.defaultProps = {
    checked: false,
    value: '',
    onChange: () => { },
}

Checkbox.propTypes = {
    checked: PropTypes.bool,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired
}

export default Checkbox
