import PropTypes from 'prop-types'



import './RangeSlider.css'

function RangeSlider(props) {

    const attributes = Array.isArray(props.attribute) ? props.attribute : [];


    const listAttributes = attributes.map((attribute, index) =>
        <div key={index} className="range_container">
            <section className="filter-category">
                <label htmlFor="fear_factor-filter">{attribute.name}</label>
                <input
                    onChange={props.onChange}
                    data-name={attribute.name}
                    id={attribute.name + "-min"}
                    className="fromSlider"
                    type="range"
                    defaultValue={0}
                    min={0}
                    max={100}
                />
                <input
                    onChange={props.onChange}
                    data-name={attribute.name}
                    id={attribute.name + "-max"}
                    type="range"
                    defaultValue={100}
                    min={0}
                    max={100}
                />
            </section>
            <article className="slider-value-sec">
                <label className="slider-value" id={attribute.name + "-min-val"}>0</label>
                <label className="slider-value" id={attribute.name + "-max-val"}>100</label>
            </article>
        </div>
    )

    return (
        <>
            {listAttributes}
        </>
    )
}

RangeSlider.defaultProps = {
    attribute: [],
    onChange: () => { }
}

RangeSlider.propTypes = {
    attribute: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string
        })
    ),
    onChange: PropTypes.func
}

export default RangeSlider


