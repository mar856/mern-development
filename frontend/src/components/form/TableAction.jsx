import PropTypes from 'prop-types'
import { Button, ButtonGroup } from "react-bootstrap"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCircleCheck, faCircleXmark, faEye, faTrashAlt } from "@fortawesome/free-regular-svg-icons"

import { MdOutlineRestore } from "react-icons/md";

function TableAction({ status, id, isAdmin, isActive, userId, onClick }) {

    const SERVER_URL = import.meta.env.VITE_SERVER_URL

    const STATUS_PENDING = 'pending'
    const STATUS_APPROVED = 'approved'

    return (
        <ButtonGroup>
            {
                status == STATUS_PENDING
                    ? <>
                        <Button
                            type="button"
                            data-link={SERVER_URL + '/dashboard/approve-character'}
                            data-id={id}
                            data-userid={userId}
                            variant="success"
                            title="Approve"
                            alt="Approve"
                            onClick={onClick}
                        >
                            <FontAwesomeIcon icon={faCircleCheck} />
                        </Button>
                        <Button
                            type="button"
                            data-link={SERVER_URL + '/dashboard/reject-character'}
                            data-id={id}
                            data-userid={userId}
                            variant="danger"
                            title="Reject"
                            alt="Reject"
                            onClick={onClick}
                        >
                            <FontAwesomeIcon icon={faCircleXmark} />
                        </Button>
                    </>
                    : ''
            }
            {
                isAdmin && status == STATUS_APPROVED
                    ?
                    isActive
                        ? <Button
                            type="button"
                            data-link={SERVER_URL + '/dashboard/archive-character'}
                            data-id={id}
                            data-userid={userId}
                            variant="warning"
                            title="Restore"
                            alt="Restore"
                            onClick={onClick}
                        >
                            <FontAwesomeIcon icon={faTrashAlt} />
                        </Button>
                        : <Button
                            type="button"
                            data-link={SERVER_URL + '/dashboard/restore-character'}
                            data-id={id}
                            data-userid={userId}
                            variant="success"
                            title="Restore"
                            alt="Restore"
                            onClick={onClick}
                        >
                            <MdOutlineRestore size={20} />
                        </Button>
                    : ''
            }
            <Button
                type="button"
                href={'/cDisplay/' + id}
                data-id={id}
                data-userid={userId}
                variant="info"
                title="Preview"
                alt="Preview"
                onClick={onClick}
            >
                <FontAwesomeIcon icon={faEye} />
            </Button>
        </ButtonGroup>
    )
}

TableAction.defaultProps = {
    status: 'pending',
    isAdmin: false,
    id: '#',
    userId: '#',
    onClick: () => { }
}

TableAction.propTypes = {
    status: PropTypes.string,
    isAdmin: PropTypes.bool,
    id: PropTypes.string,
    userId: PropTypes.string,
    onClick: PropTypes.func.isRequired
}

export default TableAction
