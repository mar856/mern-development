import React, { useEffect, useMemo, useState } from 'react'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEye } from "@fortawesome/free-regular-svg-icons"
import { faEyeSlash } from '@fortawesome/free-solid-svg-icons'
import characters from '../../../../backend/assets/characters.json'
import "./characterDisplay.css"
function Character() {
    const [characterArray, setCharacterArray] = useState([])
      
    
    useEffect(() => {
        setCharacterArray(characters)
    }, [])
    
    const changeActive = (item)=>{
        
        console.log(characterArray)
        const status = item.active ? "Deactivate" : "Activate"
        const confirmation = window.confirm(`Are you sure you want to ${status} ${item.name}?`);
        const value = !item.id
        if (confirmation){
            if(status === "Deactivate"){
                setCharacterArray(old => old.map(character => {
                    if(character.id === item.id){
                        character.active = false
                    }
                    return character
                }))
            } else {
                setCharacterArray(old => old.map(character => {
                    if(character.id === item.id){
                        character.active = true
                    }
                    return character
                }))
            }
        }
        console.log(characterArray)
    }
    return (
        <>
            {/* <input type="text" placeholder='Search User....' className='user-search'/> */}
            <table className='historytable'>
                <thead>
                    <tr>
                        <th>Name</th> 
                        <th>Active</th>
                    </tr>
                </thead>

                <tbody>
                    {characterArray.map((item,index)=> (
                        <tr key={index}>
                            <td>{item.name}</td>
                            <td><button className={item.active ? 'active-green' : 'active-red'} onClick={() => changeActive(item)}><FontAwesomeIcon icon={item.active ? faEye : faEyeSlash} /></button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}

Character.propTypes = {}

export default Character
