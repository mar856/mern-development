import { Col, Container, Row } from "react-bootstrap"
import './Header.css'

function Header() {
    return (
        <>
            <Container as="header" xxl={12}>
                <Row>
                    <Col>
                        <h1>Cartoonopia</h1>
                        <p>The home of characters and cartoons</p>
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default Header