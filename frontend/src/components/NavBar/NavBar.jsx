import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faBell } from "@fortawesome/free-regular-svg-icons"
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap"
import Notification from "../Notification/Notification"
import './NavBar.css'

function NavBar() {
    const notifications = [
        { url: "#", message: "ABC" },
        { url: "#", message: "ABC" },
        { url: "#", message: "ABC" },
        { url: "#", message: "ABC" },
    ]
    return (
        <>
            <Navbar>
                <Container>
                    <Nav className="me-auto">
                        <Nav.Link href="dashboard">Home</Nav.Link>
                        <Nav.Link href="users">Users</Nav.Link>
                        <Nav.Link href="characters">Characters</Nav.Link>
                        <Nav.Link href="newcharacter">New Character</Nav.Link>
                    </Nav>
                    <Nav>
                        <Nav.Link href="admin">Admin</Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
        </>
    )
}

export default NavBar
