import React from 'react'

import { Navigate, Route, Routes } from 'react-router-dom'

import Admin from '../views/ACP/ACP'
import User from '../views/UserList/User'
import Dashboard from "../views/Main/Main"
import LoginPage from "../views/Login/LoginForm"
import Character from "../views/Character/Character"
import RegisterPage from "../views/Register/RegisterForm"
import NewCharacter from "../views/NewCharacter/NewCharacter"
import { AuthProvider } from '../authentication/Authentication'
import AuthenticateRoute from '../authentication/AuthenticateRoute'
import CharacterDetailsPage from "../views/CharacterDisplay/cDisplay"

function Router() {
    return (
        <AuthProvider>
            <Routes>
                <Route path="/" element={
                    <AuthenticateRoute>
                        <Navigate to="/dashboard" />
                    </AuthenticateRoute>
                } />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/register" element={<RegisterPage />} />
                <Route path="/dashboard" element={
                    <AuthenticateRoute>
                        <Dashboard />
                    </AuthenticateRoute>
                } />
                <Route path="/users" element={
                    <AuthenticateRoute>
                        <User />
                    </AuthenticateRoute>
                } />
                <Route path="/characters" element={
                    <AuthenticateRoute>
                        <Character />
                    </AuthenticateRoute>
                } />
                <Route path="/admin" element={
                    <AuthenticateRoute>
                        <Admin />
                    </AuthenticateRoute>
                } />
                <Route path="/newCharacter" element={
                    <AuthenticateRoute>
                        <NewCharacter />
                    </AuthenticateRoute>
                } />
                <Route path="/cDisplay/:id" element={
                    <AuthenticateRoute>
                        <CharacterDetailsPage />
                    </AuthenticateRoute>
                } />
            </Routes>
        </AuthProvider>
    )
}


export default Router
