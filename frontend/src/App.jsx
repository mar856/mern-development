import { useEffect, useState } from "react";

import { Container } from "react-bootstrap"
import { BrowserRouter } from "react-router-dom";

import ACP from "./views/ACP/ACP"
import Router from "./route/Router";
import Header from './components/Header/Header'
import NavBar from "./components/NavBar/NavBar";
import { AuthProvider } from "./authentication/Authentication";

import "bootstrap/dist/css/bootstrap.css"

import './App.css'

function App() {
  const [showNavbar, setShowNavbar] = useState(false)
  const location = window.location.pathname

  const check = (url) => {
    return !['/login', '/register'].includes(url)
  }

  // useEffect(() => {
  //   setShowNavbar(check(location))
  // }
  //   , [location]
  // )
  return (
    <>
      {/* {showNavbar && <NavBar />} not working rn until refresh,doesnt display the navbar.,*/ }
      <NavBar />
      <Header></Header>
      <Container as="div" className="background-image-blur-whitewash" />

      <AuthProvider>
        <BrowserRouter>
          <Router />
        </BrowserRouter>
      </AuthProvider>
    </>
  )
}

export default App
