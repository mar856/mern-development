import React, { useEffect, useMemo, useState } from 'react'

import axios from 'axios'
import { Col, Container, Row } from 'react-bootstrap'

import Table from '../../components/table/Table'
import Search from '../../components/form/search/Search'
import TableAction from '../../components/form/TableAction'
import { useNavigate } from "react-router-dom";

function Character() {
    const SERVER_URL = import.meta.env.VITE_SERVER_URL;

    const navigate = useNavigate();
    useEffect(() => {
        const token = sessionStorage.getItem('sessionToken');
        if (!token) {
            navigate("/login");
        } else {
            const storedUserId = sessionStorage.getItem('userId');
            if (!storedUserId) {
                navigate("/login");
            }
        }
    }, [navigate]);

    const [tableKey, setTableKey] = useState(0)
    const [characterArray, setCharacterArray] = useState([])
    const [useFilteredData, setUseFilteredData] = useState(false)
    const [filteredCharacterArray, setFilteredCharacterArray] = useState([])

    const characterColumns = useMemo(
        () => [
            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: 'Strength',
                accessor: 'strength',
            },
            {
                Header: 'Speed',
                accessor: 'speed',
            },
            {
                Header: 'Skill',
                accessor: 'skill',
            },
            {
                Header: 'Fear Factor',
                accessor: 'fear_factor',
            },
            {
                Header: 'Power',
                accessor: 'power',
            },
            {
                Header: 'Intelligence',
                accessor: 'intelligence',
            },
            {
                Header: 'Wealth',
                accessor: 'wealth',
            },
            {
                Header: 'Action',
                accessor: 'action',
            },
        ],
        []
    );

    const characterActionEventListener = async (event) => {
        try {
            const data = event.currentTarget.dataset

            const response = await axios.post(data.link, data)
            if (response.status == 200) {
                fetchData()
            }

        } catch (error) {
            console.error(error)
        }
    }

    const characterData = useMemo(() => {
        const dataToUse = useFilteredData ? filteredCharacterArray : characterArray
        return dataToUse.map(character => ({
            ...character,
            action: <TableAction
                status="approved"
                isActive={character.isActive}
                isAdmin={false}
                id={character._id}
                userId={sessionStorage.getItem('userId')}
                onClick={characterActionEventListener}
            />,
        }))
    }, [characterArray, filteredCharacterArray, useFilteredData]
    )

    const fetchData = async () => {
        try {
            const response = await axios.get(`${SERVER_URL}/character/get-characters`);
            setCharacterArray(response.data);
            setTableKey(prevKey => prevKey + 1)
            // updateTable()
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const rowEventListener = () => { }
    const searchEventListener = (event) => {
        const filtered = characterArray.filter(character => {
            let pass = true;
            // search character
            const searchInput = event.target.value.toLowerCase();
            const characterName = character.name.toLowerCase();

            if (searchInput && searchInput.trim() !== '') {
                pass = characterName.includes(searchInput);
            }
            return pass
        })
        setFilteredCharacterArray(filtered)
        setUseFilteredData(true)
        setTableKey(prevKey => prevKey + 1)
    }
    return (
        <>
            <Container>
                <Row>
                    <Col md={{ span: 8, offset: 2 }}>
                        <Search onKeyup={searchEventListener} />
                        <Table
                            key={tableKey}
                            columns={characterColumns}
                            data={characterData}
                            onClick={rowEventListener}
                        />
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default Character
