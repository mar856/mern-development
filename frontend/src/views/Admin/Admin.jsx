import React, { useEffect, useMemo, useState } from 'react'

import axios from 'axios'
import { Col, Container, Row } from 'react-bootstrap'

import { faEye } from "@fortawesome/free-regular-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

import Button from '../../components/Button'
import Table from '../../components/table/Table'
import Search from '../../components/form/search/Search'

function History() {
    const SERVER_URL = import.meta.env.VITE_SERVER_URL

    const [useFilteredData, setUseFilteredData] = useState(false)
    const [filteredcontributionArray, setFilteredcontributionArray] = useState([])
    const [contributionArray, setcontributionArray] = useState([])
    const [tableKey, setTableKey] = useState(0)

    const fetchData = async () => {
        console.log('fetching data')
        try {
            const response = await axios.get(`${SERVER_URL}/contribution/getContribution`);
            // const response = await axios.get(`${SERVER_URL}/contribution/getContribution/1/50`);
            setcontributionArray(response.data.contributions);
            setTableKey(prevKey => prevKey + 1)
            // updateTable()
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const rowEventListener = (event) => {
        console.log(event.target)
    }

    const historyColumns = useMemo(
        () => [
            {
                Header: 'Message',
                accessor: 'message',
            },
            {
                Header: 'Preview',
                accessor: 'action',
            },
        ],
        []
    )

    const searchEventListener = (event) => {
        const filtered = contributionArray.filter(history => {
            let pass = true;
            // search history
            const searchInput = event.target.value.toLowerCase();
            const historyMessage = `${history.createdBy} ${history.actionType}`.toLowerCase();
            // const historyMessageLoweredCase = historyMessage.toLocaleLowerCase();

            if (searchInput && searchInput.trim() !== '') {
                pass = historyMessage.includes(searchInput);
            }
            return pass
        })
        setFilteredcontributionArray(filtered)
        setUseFilteredData(true)
        setTableKey(prevKey => prevKey + 1)
    }

    const historyData = useMemo(
        () => {
            const dataToUse = useFilteredData ? filteredcontributionArray : contributionArray
            return dataToUse.map(history => ({
                ...history,
                message: `${history.createdBy} ${history.actionType}`,
                action: <Button
                    title={<FontAwesomeIcon icon={faEye} />}
                    link='http://localhost:5173/changes/sdfg'
                />,
            }))
        },
        [contributionArray, filteredcontributionArray, useFilteredData]
    )

    return (
        <>
            <Container>
                <Row>
                    <Col md={{ span: 8, offset: 2 }}>
                        <Search onKeyup={searchEventListener} />
                        <Table
                            key={tableKey}
                            columns={historyColumns}
                            data={historyData}
                            onClick={rowEventListener}
                        />
                    </Col>
                </Row>
            </Container>
        </>
    )
}

History.propTypes = {}

export default History
