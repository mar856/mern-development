import { Button, Col, Container, Form, Row } from "react-bootstrap"
import InputWithLabel from "../../components/InputWithLabel"
import { useNavigate } from 'react-router-dom'
import {useEffect, useState} from 'react';
import React from 'react';
import './RegisterForm.css'
import { useAuth } from "../../authentication/Authentication";

function RegisterForm() {

    const navigate = useNavigate()
    const { login } = useAuth()

    const [loginCredentials, setLoginCredentials] = useState({
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        confirmPassword: ''
    });

    // const [passwordCheck, setPasswordCheck] = useState({
    //     password1: '',
    //     password2: '',
    // })
    const [isFormValid, setIsFormValid] = useState(false);

    useEffect(() => {
        const isValid = loginCredentials.firstName && loginCredentials.lastName && loginCredentials.email &&
            loginCredentials.password && (loginCredentials.password === loginCredentials.confirmPassword);
        setIsFormValid(isValid);
    }, [loginCredentials]);

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setLoginCredentials(prevCredentials => ({
            ...prevCredentials,
            [name]: value
        }));
    };

    const submitData = (e) => {
        e.preventDefault();
        if (!isFormValid) {
            alert("Please ensure all fields are filled correctly.");
            return;
        }

        sendData({
            firstName: loginCredentials.firstName,
            lastName: loginCredentials.lastName,
            email: loginCredentials.email,
            password: loginCredentials.password
        });
    };

    // const registerData = (e, attribute) => {
    //     if (e && e.target) {
    //         const { value } = e.target;
    //         setLoginCredentials(prevData => ({
    //             ...prevData,
    //             [attribute]: value
    //         }));
    //     }
    // };
    //
    // const registerPassword = (e, attribute) => {
    //     if (e && e.target) {
    //         const { value } = e.target;
    //         setPasswordCheck(prevData => ({
    //             ...prevData,
    //             [attribute]: value
    //         }));
    //     }
    // }

    // function submitData(e) {
    //     e.preventDefault();
    //     if (passwordMatch()) {
    //         if (checkBlanks()) {
    //             console.log("sendingData")
    //             sendData()
    //         }
    //     }
    // }

    function passwordMatch() {
        const { password1, password2 } = passwordCheck;
        if (password1 !== password2 || !password1.trim()) {
            console.log("Password mismatch or empty", passwordCheck);
            alert(`Password fields are mismatched or empty.`);
            return false;
        } else {
            setLoginCredentials(prevData => ({
                ...prevData,
                password: password1 // Set password here to ensure it gets updated before sendData is called
            }));
            return true;
        }
    }

    function checkBlanks() {
        for (let key in loginCredentials) {
            if (key !== 'password' && key !== 'isAdmin' && !loginCredentials[key].trim()) {
                if (!loginCredentials[key]) {
                    alert(`${key} field is not wrong.`);
                    return false;
                }
            }
        }
        return true;
    }

    const sendData = (loginCredentials) => {
        console.log("Sending data:", loginCredentials);

        fetch('http://localhost:3000/user/signup', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(loginCredentials),
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to register user');
                }
                return response.json();
            })
            .then(data => {
                sessionStorage.setItem('sessionToken', data.token);
                sessionStorage.setItem('userId', data.userId);
                login();
                navigate('/dashboard');
            })
            .catch(error => {
                console.error('Error in registration:', error.message);
            });
    };

    return (
        <>
            <Container fluid className="register-container">
                <h2>Register</h2>
                <Form onSubmit={submitData}>
                    <Row>
                        <Col>
                            <InputWithLabel
                                name='firstName'
                                type='text'
                                value={loginCredentials.firstName}
                                onChange={handleInputChange}
                            />
                        </Col>
                        <Col>
                            <InputWithLabel
                                name='lastName'
                                type='text'
                                value={loginCredentials.lastName}
                                onChange={handleInputChange}
                            />
                        </Col>
                    </Row>
                    <InputWithLabel
                        name='email'
                        type='email'
                        value={loginCredentials.email}
                        onChange={handleInputChange}
                    />
                    <InputWithLabel
                        name='password'
                        type='password'
                        value={loginCredentials.password}
                        onChange={handleInputChange}
                    />
                    <InputWithLabel
                        name='confirmPassword'
                        type='password'
                        value={loginCredentials.confirmPassword}
                        onChange={handleInputChange}
                    />
                    <Button type="submit" disabled={!isFormValid} className='testButton'>
                        Sign up
                    </Button><br/>
                    <Form.Text>Already have an account? <a href="/login">Login</a></Form.Text>
                </Form>
            </Container>
        </>
    )
}

export default RegisterForm
