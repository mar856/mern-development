import { useEffect, useState } from "react";

import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Container, Form } from "react-bootstrap"
import { Button, ButtonGroup, Col, Row } from "react-bootstrap"

import InputWithLabel from "../../components/InputWithLabel"
import { useAuth } from "../../authentication/Authentication";

import './LoginForm.css'

function LoginForm() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();
    const { login } = useAuth()

    useEffect(() => {
        const token = sessionStorage.getItem('sessionToken');
        if (token) {
            navigate('/dashboard');
        }
    }, [navigate]);

    const handleLogin = async (event) => {
        event.preventDefault();
        try {
            const response = await axios.post('http://localhost:3000/user/login', {
                email: email,
                password: password
            });
            if (response.data) {
                sessionStorage.setItem('sessionToken', response.data.token);  // Save the session token
                sessionStorage.setItem('userId', response.data.userId);       // Save the user ID
                console.log('Login successful and session saved!');
                login()
                navigate('/dashboard');
            }
        } catch (error) {
            console.error('Login failed:', error.response || error.message);
        }
    };

    return (
        <>
            <Container className="login-container">
                <Row>
                    <Col>
                        <h2>Login</h2>
                        <Form onSubmit={handleLogin} >
                            <InputWithLabel
                                name="email"
                                type="email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                            />
                            <InputWithLabel
                                name="password"
                                type="password"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                            />
                            <Button
                                type="submit"
                                title="sign in"
                                onClick={handleLogin}
                            >
                                Sign in
                            </Button>
                        </Form>
                        <Form.Text>Don't have an account? <a href="/register">Sign Up</a></Form.Text>
                    </Col>
                </Row>
            </Container>
        </>
    )
}
export default LoginForm