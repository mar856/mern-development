import React, { useEffect, useMemo, useState } from 'react'

import axios from 'axios'
import { Button, Col, Container, Row } from 'react-bootstrap'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faSquareCheck, faSquareXmark } from "@fortawesome/free-solid-svg-icons"

import Table from '../../components/table/Table'
import Search from '../../components/form/search/Search'
import StatusIcon from '../../components/StatusIcon/StatusIcon'

import './User.css'

function User() {
	const SERVER_URL = import.meta.env.VITE_SERVER_URL

	const [userArray, setUserArray] = useState([])
	const [tableKey, setTableKey] = useState(0)
	const [useFilteredData, setUseFilteredData] = useState(false)
	const [filteredUserArray, setFilteredUserArray] = useState([])
	const [adminArray, setAdminArray] = useState([])
	const [isAdmin, setIsAdmin] = useState(false)

	useEffect(() => {
		fetch()
		fetchAdmin()
		checkIsAdmin()
	}, []
	)

	const fetchAdmin = async () => {
		try {
			const response = await axios.get(`${SERVER_URL}/admin/get-all-admins`)
			if (response.status !== 200)
				throw new Error('Failed to fetch data')
			
			setAdminArray(response.data.data)
		} catch (error) {
			console.error('Error fetching data:', error)
		}
	}

	const fetch = async () => {
		try {
			const response = await axios.get(`${SERVER_URL}/user/get-all-users`)
			setUserArray(response.data)
			setTableKey(prevTableKey => prevTableKey + 1)
			// updateTable()
		} catch (error) {
			console.error('Error fetching data:', error)
		}
	}

	const tableActionListener = async (event) => {
		try {
			const data = event.currentTarget.dataset
			data.userId = data.id
			data.createdBy = sessionStorage.getItem('userId')

			const response = await axios.post(data.link, data)
			if (response.status == 200) {
				fetch()
			}

		} catch (error) {
			console.error(error)
		}
	}

	const filterUser = () => {
		const filtered = userArray.filter(user => {
			let accepted = true
			// search user
			const searchInput = document.getElementById('search-input').value.toLowerCase()
			const name = user.firstName.toLowerCase() + ' ' + user.lastName.toLowerCase()
			if (searchInput && searchInput.trim() !== '') {
				accepted = name.includes(searchInput)
			}
			return accepted
		})
		setFilteredUserArray(filtered)
		setUseFilteredData(true)
		setTableKey(prevKey => prevKey + 1)
	}

	const userColumns = useMemo(() => [
		{
			Header: 'Name',
			accessor: 'name',
		},
		{
			Header: 'Email',
			accessor: 'email',
		},
		{
			Header: 'Is Admin',
			accessor: 'isAdmin',
		},
		{
			Header: 'Action',
			accessor: 'action',
		},
	], []
	)

    const checkIsAdmin = async () => {
        try {
            const data = {
                id:sessionStorage.getItem('userId')
            }
            const response = await axios.post(`${SERVER_URL}/admin/get`, data)
            setIsAdmin(response.data.status)
            setTableKey(prevKey => prevKey + 1)
        } catch (error) {
            setIsAdmin(false)
        }
    }

	const userData = useMemo(() => {
		const dataToUser = useFilteredData ? filteredUserArray : userArray
		return dataToUser.map(user => ({
			name: `${user.firstName} ${user.lastName}`,
			email: `${user.email}`,
			isAdmin: <StatusIcon 
				status={adminArray.some(admin => admin._id === user._id) ? true : false}
			/>,
			action: sessionStorage.getItem('userId') !== user._id ? (
				!adminArray.some(admin => admin._id === user._id) ? (
					<Button
						type="button"
						data-link={SERVER_URL + '/admin/promote-user/'}
						data-id={user._id}
						variant="outline-primary"
						title="Promote to admin"
						onClick={tableActionListener}
						size="sm"
					>
						Promote to admin
					</Button>
				) : (
					<>
						<Button
							type="button"
							data-link={SERVER_URL + '/admin/demote-user/'}
							data-id={user._id}
							variant="outline-primary"
							title="Demote admin"
							onClick={tableActionListener}
							size="sm"
						>
							Demote admin
						</Button>
					</>
				)
			) : null
		}))
	}, [userArray, tableKey, filteredUserArray, useFilteredData]
	)

	return (
		<Container>
			<Row>
				<Col md={{ span: 8, offset: 2 }}>
					<Search
						name="User"
						onKeyup={filterUser}
					/>
					<Table
						key={tableKey}
						columns={userColumns}
						data={userData}
					/>
				</Col>
			</Row>
		</Container>
	)
}

export default User
