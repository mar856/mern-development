import React from 'react';
import './RoundPicture.css'

const RoundPicture = ({ src, alt }) => {
  return <img src={src} alt={alt} className="round-picture" />;
};

export default RoundPicture;
