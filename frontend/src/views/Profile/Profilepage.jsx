import React, {useEffect, useState} from 'react';
import './Profilepage.css'
import RoundPicture from './Picture/RoundPicture';
import myImage from './picture.png';
import {useNavigate} from "react-router-dom";

function Profilepage() {
    const [profileInfo, setProfileInfo] = useState({
        Picture: myImage,
        Name: '',
        Contributions: [],
        Favorites: []
    });
    const navigate = useNavigate();
    const [userId, setUserId] = useState('');

    useEffect(() => {
        const token = sessionStorage.getItem('sessionToken');
        if (!token) {
            navigate("/login");
        } else {
            const storedUserId = sessionStorage.getItem('userId');
            if (storedUserId) {
                setUserId(storedUserId);
            } else {
                navigate("/login");
            }
        }
    }, [navigate]);

    useEffect(() => {
        if (userId) {
            fetch(`http://localhost:3000/user/${userId}`, {
                method: 'GET'
            })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                setProfileInfo(prevState => ({
                    ...prevState,
                    Name: data.firstName + " " + data.lastName,
                    Favorites: data.Favorites || []
                }));
            })
            .catch(error => console.error('There was a problem with the fetch operation:', error));

            fetch(`http://localhost:3000/history/by/${userId}`, {
                method: 'GET',
            })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                const descriptions = data.map((contribution) => {
                    const actionType = contribution.actionType;
                    const characterName = contribution.version.new.name;
                    const date = contribution.date;
                    return `${actionType} ${characterName} on ${date}`;
                });
                setProfileInfo(prevState => ({
                    ...prevState,
                    Contributions: descriptions
                }));
            })
            .catch(error => console.error('There was a problem with the fetch operation:', error));
        }
    }, [userId]);

    return (
        <div >
            <div className='test'>
                <table className="profile-info">
                <tbody>
                <tr className="profile-info">
                    <td className='profile-display'>
                    <RoundPicture src={profileInfo.Picture} alt={profileInfo.name} />
                    </td>
                    <td className='profile-box'>
                    <h3 className='profile-name'>{profileInfo.Name}</h3>
                    <p>{profileInfo.Description}</p>
                    </td>
                </tr>

                <tr>
                    <div className='favorite'>
                    <h2>Favorites</h2>
                    <div>
                        {profileInfo.Favorites.map((Favorites, index) => (
                        <div key={index}>{Favorites}</div>
                    ))}</div>
                    </div>
                </tr>
                <tr> 
                    <div className='contribution'>
                    <h2>Contributions</h2>
                    <div>{profileInfo.Contributions.map((contribution, index) => (
                        <div key={index}>{contribution}</div>
                    ))}</div>
                    </div>
                </tr>
                </tbody>
                </table>
            </div>
            
           
      </div>
    )
}

export default Profilepage
