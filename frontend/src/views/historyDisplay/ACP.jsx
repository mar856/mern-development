import React, { useEffect, useMemo, useState } from 'react'

import axios from 'axios'
import { Col, Container, Row } from 'react-bootstrap'

import { faEye } from "@fortawesome/free-regular-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

import Button from '../Button'
import Table from '../table/Table'
import Search from '../form/search/Search'

import userlist from '../../../../backend/assets/userlist.json'
import contributions from '../../../../backend/assets/contributions.json'
import adminlist from '../../../../backend/assets/adminlist.json'
import characters from '../../../../backend/assets/characters.json'

import "./ACP.css"
function History() {
    const SERVER_URL = import.meta.env.VITE_SERVER_URL
    
    const [useFilteredData, setUseFilteredData] = useState(false)
    
    const [filteredHistoryArray, setFilteredHistoryArray] = useState([])
    
    // contributions
    const [historyArray, setHistoryArray] = useState([])
    // userlist
    const [userArray, setUserArray] = useState([])
    // adminlist
    const [adminArray, setAdminArray] = useState([])
    // characters
    const [characterArray, setCharacterArray] = useState([])
        
    /*
    const fetchData = async () => {
        console.log('fetching data')
        try {
            const response = await axios.get(`${SERVER_URL}/history/get-history/1/50`);
            setHistoryArray(response.data.histories);
            setTableKey(prevKey => prevKey + 1)
            
            console.log(historyArray)
            // updateTable()
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }


    useEffect(() => {
        fetchData()
    }, [])
    */
    
    useEffect(() => {
        setHistoryArray(contributions),
        setUserArray(userlist),
        setCharacterArray(characters),
        setAdminArray(adminArray)
    }, [])

    const rowEventListener = (event) => {
        console.log(event.target)
    }

    const historyColumns = useMemo(
        () => [
            {
                Header: 'Message',
                accessor: 'message',
            },
            {
                Header: 'Preview',
                accessor: 'action',
            },
        ],
        []
    )

    const searchEventListener = (event) => {
        const filtered = historyArray.filter(history => {
            let pass = true;
            // search history
            const searchInput = event.target.value.toLowerCase();
            const historyMessage = `${history.createdBy} ${history.actionType}`.toLowerCase();
            // const historyMessageLoweredCase = historyMessage.toLocaleLowerCase();

            if (searchInput && searchInput.trim() !== '') {
                pass = historyMessage.includes(searchInput);
            }
            return pass
        })
        setFilteredHistoryArray(filtered)
        setUseFilteredData(true)
        setTableKey(prevKey => prevKey + 1)
    }

    // const historyData = useMemo(
    //     () => {
    //     //     const dataToUse = useFilteredData ? filteredHistoryArray : historyArray
    //     //     return dataToUse.map(history => ({
    //     //         ...history,
    //     //         message: `${history.createdBy} ${history.actionType}`,
    //     //         action: <Button
    //     //             title={<FontAwesomeIcon icon={faEye} />}
    //     //             link='http://localhost:5173/changes/sdfg'
    //     //         />,
    //     //     }))
    //     // },
    //     [historyArray, filteredHistoryArray, useFilteredData]
    // )

    // to display the names of users that changed/reviewed the history
    const findUser = (item) => {
        const targetId = item._id.$oid
        const target = userArray.find(obj => obj._id.$oid === targetId)
        console.log(target)
        return `${target.firstname} ${target.lastname}`
    }
    
    const changeStatus = (item) => {
        if (item.status === "Pending"){

            console.log("Hello")
        } else{
            console.log("No")
        }
    }
    return (
        <>
            <table className='historytable'>
                <thead>
                    <tr>
                        <th>Changed By</th> 
                        <th>Action</th>
                        <th>Status</th>
                        <th>Reviewed By</th>
                        <th>Date</th>
                        <th>Data</th>
                    </tr>
                </thead>

                <tbody>
                    {historyArray.map((item,index)=> (
                        <tr key={index}>
                            <td>{findUser(item.user_id)}</td>
                            <td>{item.action.split("Character")[0]}</td>
                            <td>{
                                <>
                                    <div onClick={() => changeStatus(item)}>
                                    {item.status}
                                    </div>
                                </>
                            }</td>
                            <td>{item.status === "Pending" ? '-' : findUser(item.reviewed_by)}</td>
                            <td>{item.date}</td>
                            <td>tbd</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}

History.propTypes = {}

export default History
