import React, { useEffect, useMemo, useState } from 'react'

import axios from 'axios'
import { useNavigate } from "react-router-dom"
import { Col, Container, Figure, Row } from 'react-bootstrap'

import { MdOutlineRestore } from "react-icons/md"
import { faEye } from "@fortawesome/free-regular-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

import Button from '../../components/Button'
import Table from '../../components/table/Table'
import Search from '../../components/form/search/Search'
import RoundPicture from '../Profile/Picture/RoundPicture'
import TableAction from '../../components/form/TableAction'
import FilterBox from '../../components/Filter/Box/FilterBox'
import Checkbox from '../../components/form/checkbox/Checkbox'
import Comparison from '../../components/Comparison/Comparison'

import './Main.css'
function Main() {
    const SERVER_URL = import.meta.env.VITE_SERVER_URL
    const navigate = useNavigate()

    const [checkedCharacter, setCheckedCharacter] = useState([])
    const [comparisonHistory, setComparisonHistory] = useState([])
    const [attributes, setAttributes] = useState([
        "strength",
        "speed",
        "skill",
        "fear-factor",
        "power",
        "intelligence",
        "wealth"
    ])
    const [appliedFilter, setAppliedFilter] = useState(
        attributes.reduce((acc, curr) => ({
            ...acc,
            [curr]: { min: 0, max: 100 }
        }), {})
    )
    const [characterArray, setCharacterArray] = useState([])
    const [useFilteredData, setUseFilteredData] = useState(false)
    const [filteredCharacterArray, setFilteredCharacterArray] = useState([])
    const [tableKey, setTableKey] = useState(0)
    const [currentPair, setCurrentPair] = useState([])
    const [isAdmin, setIsAdmin] = useState(false)

    useEffect(() => {
        // Check for session token and navigate if not authenticated
        const token = sessionStorage.getItem('sessionToken')
        if (!token) {
            navigate("/login")
        } else {
            const storedUserId = sessionStorage.getItem('userId')
            if (!storedUserId) {
                navigate("/login")
            }
        }

        fetchData()
    }, [navigate])

    useEffect(() => {
        checkIsAdmin()
    }, [])

    useEffect(() => {
        // Fetch and compare characters attribute
        if (currentPair.length === 2) {
            const [char1Id, char2Id] = currentPair
            const char1 = characterArray.find(character => character._id === char1Id)
            const char2 = characterArray.find(character => character._id === char2Id)

            if (char1 && char2) {
                const compareResult = attributes.map(attribute => {
                    let attr = (attribute == 'fear-factor') ? 'fear_factor' : attribute
                    let left = (char1[attr] > char2[attr]) ? true : false
                    let right = (char1[attr] < char2[attr]) ? true : false

                    return {
                        left: left,
                        vs: attribute,
                        right: right
                    }
                })
                setCheckedCharacter(compareResult)
                setComparisonHistory(prevHistory => [...prevHistory, { char1: char1, char2: char2 }])
            }
        }
    }, [characterArray, attributes, currentPair])

    const fetchData = async () => {
        try {
            const response = await axios.get(`${SERVER_URL}/dashboard/get-character`)
            setCharacterArray(response.data)
            setTableKey(prevTableKey => prevTableKey + 1)
            // updateTable()
        } catch (error) {
            console.error('Error fetching data:', error)
        }
    }

    const checkboxEventListener = (event) => {
        const { checked, value } = event.target

        setCurrentPair(existingCharacter => {
            if (checked) {
                return [...existingCharacter, value]
            }
            else {
                return existingCharacter.filter(val => val !== value)
            }
        })
    }

    const filterRangeEventListener = (event) => {
        // update filter range label
        document.getElementById(event.target.id + '-val').innerHTML = event.target.value

        attributes.forEach(attribute => {
            var min = Number(document.getElementById(`${attribute}-min`).value)
            var max = Number(document.getElementById(`${attribute}-max`).value)

            setAppliedFilter(prevFilter => {
                const newFilter = { ...prevFilter }
                newFilter[attribute] = {
                    min: min,
                    max: max
                }
                return newFilter
            })
        })
        filterCharacters()
    }
    const filterCharacters = () => {
        const filtered = characterArray.filter(character => {
            let accepted = true
            Object.keys(appliedFilter).every(attribute => {
                const { min, max } = appliedFilter[attribute]

                if (attribute == 'fear-factor')
                    attribute = 'fear_factor'

                // filter character based on their attribute
                const characterVal = character[attribute]
                accepted = characterVal >= min && characterVal <= max

                // search character
                const searchInput = document.getElementById('search-input').value.toLowerCase()
                const characterName = character.name.toLowerCase()
                if (searchInput && searchInput.trim() !== '') {
                    accepted = characterName.includes(searchInput)
                }

            })
            return accepted
        })
        setFilteredCharacterArray(filtered)
        setUseFilteredData(true)
        setTableKey(prevKey => prevKey + 1)
    }

    const characterActionEventListener = async (event) => {
        try {
            const data = event.currentTarget.dataset

            const response = await axios.post(data.link, data)
            if (response.status == 200) {
                fetchData()
            }

        } catch (error) {
            console.error(error)
        }
    }

    const historyTableEventListener = (event) => {
        event.preventDefault()
        const { char1, char2 } = event.currentTarget.dataset
        setCurrentPair([char1, char2])
        setTableKey(prevKey => prevKey + 1)
    }

    const characterColumns = useMemo(
        () => [
            {
                Header: '',
                accessor: 'compare',
            },
            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: 'Strength',
                accessor: 'strength',
            },
            {
                Header: 'Speed',
                accessor: 'speed',
            },
            {
                Header: 'Skill',
                accessor: 'skill',
            },
            {
                Header: 'Fear Factor',
                accessor: 'fear_factor',
            },
            {
                Header: 'Power',
                accessor: 'power',
            },
            {
                Header: 'Intelligence',
                accessor: 'intelligence',
            },
            {
                Header: 'Wealth',
                accessor: 'wealth',
            },
            {
                Header: 'Action',
                accessor: 'action',
            },
        ],
        []
    )

    const characterData = useMemo(() => {

        const dataToUse = useFilteredData ? filteredCharacterArray : characterArray

        return dataToUse.map(character => ({
            ...character,
            compare: <Checkbox
                checked={currentPair.includes(character._id)}
                onChange={checkboxEventListener}
                value={character._id}
                disable={currentPair.length === 2 && !currentPair.includes(character._id)}
            />,
            action: <TableAction
                status="approved"
                isActive={character.active}
                isAdmin={isAdmin}
                id={character._id}
                userId={sessionStorage.getItem('userId')}
                onClick={characterActionEventListener}
            />,
        }))
    }, [characterArray, filteredCharacterArray, useFilteredData, currentPair]
    )

    const detailedComparisonColumns = useMemo(
        () => [
            {
                Header: '',
                accessor: 'left',
            },
            {
                Header: '',
                accessor: 'vs',
            },
            {
                Header: '',
                accessor: 'right',
            },
        ],
        []
    )

    const detailedComparisonData = useMemo(
        () => {
            if (currentPair.length !== 2) {
                return attributes.map(attribute => ({
                    left: '',
                    vs: attribute,
                    right: '',

                }))
            }
            if (checkedCharacter.length > 0) {
                return checkedCharacter.map(result => ({
                    left: <Comparison status={result.left} />,
                    vs: result.vs,
                    right: <Comparison status={result.right} />
                }))
            }
            else {
                return attributes.map(attribute => ({
                    left: '',
                    vs: attribute,
                    right: '',

                }))
            }
        }, [checkedCharacter, attributes, currentPair]
    )

    const comparisonHistoryColumns = useMemo(
        () => [
            {
                Header: '',
                accessor: 'action',
            },
            {
                Header: '',
                accessor: 'left',
            },
            {
                Header: '',
                accessor: 'right',
            }
        ],
        []
    )

    const comparisonHistoryData = useMemo(() => {
        return comparisonHistory.map((history, index) => ({
            action: <Button
                title={<MdOutlineRestore size={20} />}
                size="sm"
                variant="outline-secondary"
                onClick={historyTableEventListener}
                char1={history.char1._id}
                char2={history.char2._id}
                type='button'
            />,
            left: history.char1.name,
            right: history.char2.name,
        }))
    }, [comparisonHistory]
    )

    const checkIsAdmin = async () => {
        try {
            const data = {
                id: sessionStorage.getItem('userId')
            }
            const response = await axios.post(`${SERVER_URL}/admin/get`, data)
            setIsAdmin(response.data.status)
            setTableKey(prevKey => prevKey + 1)
        } catch (error) {
            setIsAdmin(false)
        }
    }

    return (
        <>
            <Container as="section" className="user-inputs">
                <Row>
                    <Col m={1}>
                        <FilterBox
                            onChange={filterRangeEventListener}
                        />
                    </Col>
                    <Col>
                        <Container as="section" id="searching">
                            <Search
                                name="Character"
                                onKeyup={filterCharacters}
                            />
                            <Table
                                key={tableKey}
                                columns={characterColumns}
                                data={characterData}
                            />
                        </Container>
                    </Col>
                    <Col m={1}>
                        <Container as="section" id="previous-comparisons">
                            <h3>Previous Comparisons</h3>
                            <Table
                                columns={comparisonHistoryColumns}
                                data={comparisonHistoryData}
                                header={false}
                                pagination={false}
                            />
                        </Container>
                    </Col>
                </Row>
            </Container>
            <Container as="section" className='image-comparison'>
                {
                    currentPair.length === 2 ? (
                        currentPair.map((charId, index) => {
                            const character = characterArray.find(char => char._id === charId)
                            return character ? (
                                <>
                                    <Figure key={index} id={`character-image-${index === 0 ? 'left' : 'right'}`} className='character-image'>
                                        <Figure.Caption>{character.name}</Figure.Caption>
                                        <RoundPicture src={character.image_url} alt={character.name} />
                                        {/* <img src={character.image_url} alt={character.name} /> */}
                                    </Figure>
                                    {(index === 0) && (<span id='vs-text'>vs</span>)}
                                </>
                            ) : null
                        })
                    ) : (
                        <>
                            <Figure id='character-image-right' className='character-image'>
                                <Figure.Caption>Unknown</Figure.Caption>
                                <div className='unknown-image'>?</div>
                            </Figure>
                            <Figure id='character-image-right' className='character-image'>
                                <Figure.Caption>Unknown</Figure.Caption>
                                <div className='unknown-image'>?</div>
                            </Figure>
                        </>
                    )
                }
            </Container>
            <Container id="detailed-comparison">
                <div id="comparison-table">
                    <Table
                        columns={detailedComparisonColumns}
                        data={detailedComparisonData}
                        pagination={false}
                        header={false}
                        onClick={historyTableEventListener}
                    />
                </div>
            </Container>
        </>
    )
}

export default Main
