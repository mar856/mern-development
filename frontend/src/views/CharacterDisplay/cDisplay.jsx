import { useState } from 'react';
import React, { useEffect } from 'react';

import axios from 'axios'
import { useNavigate, useParams } from "react-router-dom";

import ReactButton from 'react-bootstrap/Button';
import { faStar } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

import myImage from './picture.png';
import RoundPicture from '../Profile/Picture/RoundPicture';

import './cDisplay.css'

function cDisplay() {
    
    //Todo
    // Character id to load data
    // pull from mongodb
    // search for id

    // Alert if character has already been edited
    // fetch from mongodb
    // sort by date and time descending
    // for each contribution, 
    // if status is approved
    //      is id == character viewed
    //          break
    //  if status is pending
    //      if id == character viewed
    //          disable edit.
    const SERVER_URL = import.meta.env.VITE_SERVER_URL
    
    const {id} = useParams()
    const navigate = useNavigate();
    
    const [userId, setUserId] = useState('');
    const [data, setData] = useState([])
    const [isEditing, setIsEditing] = useState(false);
    const [confirmData,setIsConfirm] = useState(false);
    const [profileInfo, editprofileInfo] = useState([]);

    useEffect(() => {
        getCharacter()
    }, [])
    
    const getCharacter = async () => {
        try {
            const response = await axios.get(`${SERVER_URL}/character/get/${id}`)
            if (response.status !== 200) {
                console.error(response.message)
            }
            setData(response.data) // Assuming the data you want is in response.data
            editprofileInfo(response.data)
        } catch (error) {
            console.error('Error, fetching character information', error)
        }
    }

    // useEffect(() => {
    //     const token = sessionStorage.getItem('sessionToken');
    //     if (!token) {
    //         navigate("/login");
    //     } else {
    //         const storedUserId = sessionStorage.getItem('userId');
    //         if (storedUserId) {
    //             setUserId(storedUserId);
    //         } else {
    //             navigate("/login");
    //         }
    //     }
    // }, [navigate]);
    
    // const userId = "662e06bcedb0bc192a951283";
    const PENDING_STATUS = "pending";
    const ADD_CHARACTER = "AddCharacter"
    const previousComparison = ["batman,superman","Loki,Thor"]

    function favouriteCharacter(name){
        console.log(name, "has been favourited")
        //Get username
        //Return (username, charactername) to database
    }

    //Handles updating edit button
    const handleEditClick = () => {
        if(isEditing){
            console.log('Edited Profile Info:', profileInfo);
            if (JSON.stringify(data) !== JSON.stringify(profileInfo)) {
                setIsConfirm(true);
            }
        }
        setIsEditing(!isEditing);
    };

    //Save data to state
    const handleInputChange = (event, attribute) => {
        const { value } = event.target;
        var filter = ["name","subtitle"]
        if (filter.includes(attribute)){
            editprofileInfo(prevState => ({
                ...prevState,
                [attribute]: String(value),
                }));
        }

        else{
            editprofileInfo(prevState => ({
            ...prevState,
            [attribute]: Number(value),
            }));
        }
    };
    
    //Api routing, sends data to backend with status pending.
    function updateData() {
        setIsConfirm(false);
        const todayDate = new Date()
        const formattedData = {
            status: PENDING_STATUS,
            user_id: userId,
            reviewed_by: null,
            action: ADD_CHARACTER,
            date: todayDate,
            data: {
                old: data,
                new: profileInfo
            }
        };
        console.log(formattedData)

        fetch('http://localhost:3000/contribution/update', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formattedData),
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to update data');
            }
            return response.json();
        })
        .then(data => {
            console.log('Data updated successfully:', data);
            // Update frontend UI as needed
        })
        .catch(error => {
            console.error('Error updating data:', error.message);
            // Display error message to user or handle error gracefully
        });
    }
    
    return (
        <div className='cDisplay'>
            <table>
                <tbody>
                <tr>
                <td className='cDisplay-body'> {/* this is the main content */}
                    <div className='cprofile-info'>
                        <table >
                            <tbody>
                            <tr>
                                <td>
                                <RoundPicture src={profileInfo.picture} alt={profileInfo.name} />
                                </td>
                                <td>
                                <h3>
                                    {isEditing ? (
                                        <input
                                        type='String'
                                        value={profileInfo.name}
                                        onChange={e => handleInputChange(e, 'name')}
                                        />
                                    ) : (profileInfo.name)} 
                                    <ReactButton title="favourite" onClick={() => favouriteCharacter(profileInfo.name)}>
                                        <FontAwesomeIcon icon={faStar} />
                                    </ReactButton>
                                </h3>
                                <p>{isEditing ? (
                                        <input
                                        type='string'
                                        value={profileInfo.subtitle}
                                        onChange={e => handleInputChange(e, 'subtitle')}
                                        />
                                    ) : (profileInfo.subtitle)}</p>
                                
                                <p> {isEditing ? (
                                        <input
                                        type='string'
                                        value={profileInfo.description}
                                        onChange={e => handleInputChange(e, 'description')}
                                        />
                                    ) : (profileInfo.description)}</p>
                                </td>
                                <td>   
                                    <button className='editButton' onClick={handleEditClick}>
                                            {isEditing ? 'Save' : 'Edit'}
                                    </button>
                                </td>
                                <td>
                                    {confirmData && (
                                        <button className='confirmButton' onClick={updateData}>
                                            {confirmData ? 'Confirm' : ''}
                                        </button>
                                    )}

                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className='cDisplay-table'>
                        <table className='attribute-table'>
                            <tbody>
                            <tr>
                                <td>
                                    <div>Strength</div>
                                    <div>
                                        {isEditing ? (
                                            <input
                                            type='number'
                                            value={profileInfo.strength}
                                            onChange={e => handleInputChange(e, 'strength')}
                                            />
                                        ) : (<div>{profileInfo.strength}</div>)}
                                    </div>
                                </td>
                                <td>
                                    <div>Speed</div>
                                    <div>
                                        {isEditing ? (
                                            <input
                                            type='number'
                                            value={profileInfo.speed}
                                            onChange={e => handleInputChange(e, 'speed')}
                                            />
                                        ) : (<div>{profileInfo.speed}</div>)}
                                    </div>
                                </td>
                                <td>
                                    <div>Skill</div>
                                    <div>
                                        {isEditing ? (
                                            <input
                                            type='number'
                                            value={profileInfo.skill}
                                            onChange={e => handleInputChange(e, 'skill')}
                                            />
                                        ) : (<div>{profileInfo.skill}</div>)}
                                    </div>
                                </td>
                                <td>
                                    <div>Fear</div>
                                    <div>
                                        {isEditing ? (
                                            <input
                                            type='number'
                                            value={profileInfo.fear_factor}
                                            onChange={e => handleInputChange(e, 'fear_factor')}
                                            />
                                        ) : (<div>{profileInfo.fear_factor}</div>)}
                                    </div>
                                </td>
                            </tr>
                            <tr className='second-row'>
                                <td>
                                    <div>Power</div>
                                    <div>
                                        {isEditing ? (
                                            <input
                                            type='number'
                                            value={profileInfo.power}
                                            onChange={e => handleInputChange(e, 'power')}
                                            />
                                        ) : (<div>{profileInfo.power}</div>)}
                                    </div>
                                    </td>
                                <td>
                                    <div>Intelligence</div>
                                    <div>
                                        {isEditing ? (
                                            <input
                                            type='number'
                                            value={profileInfo.intelligence}
                                            onChange={e => handleInputChange(e, 'intelligence')}
                                            />
                                        ) : (<div>{profileInfo.intelligence}</div>)}
                                    </div>
                                </td>
                                <td>
                                
                                    <div>Wealth</div>
                                    <div>
                                        {isEditing ? (
                                            <input
                                            type='number'
                                            value={profileInfo.wealth}
                                            onChange={e => handleInputChange(e, 'wealth')}
                                            />
                                        ) : (<div>{profileInfo.wealth}</div>)}
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                </td>
                </tr>
                </tbody>
          </table>
      </div>
    )
}

export default cDisplay
