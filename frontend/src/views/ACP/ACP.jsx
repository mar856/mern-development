import React, { useEffect, useMemo, useState } from 'react'

import axios from 'axios'
import { useNavigate } from 'react-router-dom'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPenToSquare } from "@fortawesome/free-solid-svg-icons"

import UserDisplay from '../../components/AdminUser/userDisplay'
import HistoryDisplay from '../../components/AdminHistory/historyDisplay'
import CharacterDisplay from '../../components/AdminCharacter/characterDisplay'

import "./ACP.css"

function ACP() {
        const SERVER_URL = import.meta.env.VITE_SERVER_URL
        const navigate = useNavigate()

        const [showHistory, setshowHistory] = useState(false)
        const [showUser, setshowUser] = useState(false)
        const [showCharacter, setshowCharacter] = useState(false)


        useEffect(() => {
            const checkIsAdmin = async () => {
                try {
                    const data = {
                        id:sessionStorage.getItem('userId')
                    }
                    const response = await axios.post(`${SERVER_URL}/admin/get`, data)
                    if (response.status === 403) {
                        alert('Forbidden: You are not authorized on this page')
                        setTimeout(() => {
                            navigate('/dashboard')
                        }, 4000)
                    }
                } catch (error) {
                    console.error(error.message)
                    // Navigate to dashboard or another error page in case of an error
                    navigate('/dashboard')
                }
            }
            checkIsAdmin()
        }, [navigate])

        const changehistory = () => {
            setshowHistory(!showHistory)
            setshowUser(false)
            setshowCharacter(false)
        }

        const changeuser = () => {
            setshowUser(!showUser)
            setshowHistory(false)
            setshowCharacter(false)
        }
        
        const changecharacter = () => {
            setshowCharacter(!showCharacter)
            setshowHistory(false)
            setshowUser(false)
        }

        return (
            <>
                <div className='AdminPanel'>
                    <div className='functions'>
                        <button onClick={changeuser}><FontAwesomeIcon icon={faPenToSquare} className='icon'/>User</button>
                        <button onClick={changecharacter}><FontAwesomeIcon icon={faPenToSquare} className='icon' />Character</button>
                        <button onClick={changehistory}><FontAwesomeIcon icon={faPenToSquare} className='icon' />History</button>
                    </div>

                    <div className='display'>
                        {showUser === true ? <UserDisplay/> : null}
                        {showHistory === true ? <HistoryDisplay/> : null}
                        {showCharacter === true ? <CharacterDisplay/> : null}
                    </div>
                </div>
            </>
    )
}

ACP.propTypes = {}

export default ACP
