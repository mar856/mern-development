import { Col, Container, Form, Row } from "react-bootstrap"
import InputWithLabel from "../../components/InputWithLabel"
import Button from "../../components/Button"
import './NewCharacter.css'
import React, {useEffect, useState} from 'react';
import {useNavigate} from "react-router-dom";

function NewCharacterForm() {
    const [url, setUrl] = useState('');
    const [isValidImage, setIsValidImage] = useState(true);

    // const navigate = useNavigate();
    // useEffect(() => {
    //     const token = sessionStorage.getItem('sessionToken');
    //     if (!token) {
    //         navigate("/login");
    //     } else {
    //         const storedUserId = sessionStorage.getItem('userId');
    //         if (!storedUserId) {
    //             navigate("/login");
    //         }
    //     }
    // }, [navigate]);

    function onSubmit(){
        const character = {
            isActive: false,
            name: document.querySelector('input[name="name"]').value,
            subtitle: document.querySelector('input[name="subtitle"]').value,
            description: document.querySelector('input[name="description"]').value,
            strength: document.querySelector('input[name="strength"]').value,
            speed: document.querySelector('input[name="speed"]').value,
            skill: document.querySelector('input[name="skill"]').value,
            fear_factor: document.querySelector('input[name="fear-factor"]').value,
            power: document.querySelector('input[name="power"]').value,
            intelligence: document.querySelector('input[name="intelligence"]').value,
            wealth: document.querySelector('input[name="wealth"]').value
        }

        fetch('http://localhost:3000/character/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(character),
        })
        .then(response => {

            if (!response.ok) {
                throw new Error('Failed to update data');
            }
            return response.json();
        })
        .then(data => {
            console.log('Data updated successfully:', data);
            // Update frontend UI as needed
        })
        .catch(error => {
            console.error('Error updating data:', error.message);
            // Display error message to user or handle error gracefully
        });
        
    }

    const changeUrl = async (event) => {
      const url = event.target.value;
      setUrl(url)
      setIsValidImage(false);
  
      try {
        const response = await fetch(url);
        
        if (response.ok){
            const contentType = response.headers.get('content-type');
            setIsValidImage(contentType && contentType.startsWith('image'));
            //console.log(isValidImage)
        }
        
      } catch (error) {
        setIsValidImage(false);
        console.error('Error fetching image:', error);
      }
    };

    return (
        <>
            <Container className="newcharacter-container">
                <h2>Add New Character</h2>
                <form action="">
                    <div className="main">
                        <div className="section">
                            <div className="details">
                                <label htmlFor="id">ID</label>
                                <input type="text" name="id"/>
                                <label htmlFor="name">Name</label>
                                <input type="text" name="name"/>
                                <label htmlFor="subtitle">Subtitle</label>
                                <input type="text" name="subtitle"/>
                                <label htmlFor="description">Description</label>
                                <input type="text" name="description" id="description"/>
                            </div>
                        </div>
                        <div className="section">
                            <div className="attributes"> 
                                <div>
                                    <label htmlFor="strength">Strength</label>
                                    <input type="number" name="strength" min={0} max={100}/>
                                </div>
                                <div>
                                    <label htmlFor="speed">Speed</label>
                                    <input type="number" name="speed" min={0} max={100}/>
                                </div>
                                <div>
                                    <label htmlFor="skill">Skill</label>
                                    <input type="number" name="skill" min={0} max={100}/>
                                </div>
                                <div>
                                    <label htmlFor="fear-factor">Fear Factor</label>
                                    <input type="number" name="fear-factor" min={0} max={100}/>
                                </div>

                                <div>
                                    <label htmlFor="power">Power</label>
                                    <input type="number" name="power" min={0} max={100}/>
                                </div>
                                <div>
                                    <label htmlFor="intelligence">Intelligence</label>
                                    <input type="number" name="intelligence" min={0} max={100}/>
                                </div>
                                <div  id="wealth" >
                                    <label htmlFor="wealth">Wealth</label>
                                    <input type="number" name="wealth" min={0} max={100}/>
                                </div>
                            </div>
                        </div>
                        <div className="section">
                            <div className="image">
                                <label htmlFor="img-url">Image URL</label>
                                <input type="url" name="img-url" onChange={changeUrl}/>
                                <div className="image-container">
                                    {(isValidImage || url === "") ? (
                                        <img src={url} />
                                    ) : (
                                        <p>Please enter a valid URL!</p>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                    <div>
                        <button type="submit" onClick={onSubmit}>SUBMIT</button>
                        <button type="reset" value="Reset">Reset</button>
                    </div>
            </Container>
        </>
    )
}

export default NewCharacterForm
