const UserModel = require('./models/user');  // this works because its directly using the export
const {Character} = require("./models/character");
const {Contribution, ADD_CHARACTER, EDIT_CHARACTER} = require("./models/contribution");
const bcrypt = require("bcrypt");
const fs = require('fs').promises;
const path = require('path');
const mongoose = require('mongoose');

const addMockData = async () => {
    try {
        await addCharacters();
        console.log('Success populating characters');
        await addUsersData();
        console.log('Success populating Users');
    } catch (error) {
        console.error('Error adding new data,', error);
    }
}
 
const addCharacters = async () => {
    try {
        const jsonFilePath = path.join(__dirname,'..','assets','characters.json')
        const data = await fs.readFile(jsonFilePath, 'utf8'); // Use await to wait for the file read
        const characterData = JSON.parse(data);
        const session = await mongoose.startSession();
        try {
            session.startTransaction();
            const charactersInput = characterData.map(character => ({
                active: character.active,
                name: character.name,
                subtitle: character.subtitle,
                description: character.description,
                image_url: character.image_url,
                strength: character.strength,
                speed: character.speed,
                skill: character.skill,
                fear_factor: character.fear_factor,
                power: character.power,
                intelligence: character.intelligence,
                wealth: character.wealth
            }));
            await Character.insertMany(charactersInput);
            session.commitTransaction();
            
            return true;
        } catch (error) {
            session.abortTransaction();
            console.error("Error populating character", error);
        }
    } catch (err) {
        console.error('Error reading JSON file:', err);
    }
}

const addUsersData = async () => {
    try {
        const jsonFilePath = path.join(__dirname,'..','assets','userlist.json')
        const data = await fs.readFile(jsonFilePath, 'utf8'); // Use await to wait for the file read
        const userData = JSON.parse(data);
        const session = await mongoose.startSession();
        try {
            session.startTransaction();
            const usersInput = userData.map((user, index) => {
                return {
                    firstName: user.firstname,
                    lastName: user.lastname,
                    email: user.email,
                    password: bcrypt.hashSync(user.password, bcrypt.genSaltSync())
                };
            });
            await UserModel.insertMany(usersInput);
            session.commitTransaction();
            return true;
        } catch (error) {
            session.abortTransaction();
            console.error("Error populating user", error);
        }
    } catch (err) {
        console.error('Error reading JSON file:', err);
    }
}

async function clearCollections() {
    try {
        // Clears all documents from each collection but does not drop the collections themselves
        await UserModel.deleteMany({});
        await Character.deleteMany({})
        await Contribution.deleteMany({});
        console.log('All collections cleared successfully');
    } catch (error) {
        console.error('Error clearing collections:', error);
    }
}

module.exports = { clearCollections, addMockData };