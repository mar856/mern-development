const express = require('express');
const router = express.Router();
const adminController = require('../controllers/admin.server.controller');

router.get('/get-all-admins', adminController.getAllAdmin);
router.post('/get', adminController.isAdmin);
router.post('/changeAdmin', adminController.changeAdmin);
router.post('/promote-user', adminController.promoteUser);
router.post('/demote-user', adminController.demoteUser);



module.exports = router;