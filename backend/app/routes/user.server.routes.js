const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.server.controller');

router.get('/:id([0-9a-fA-F]{24})', userController.getUser);
router.get('/get-all-users', userController.getAllUsers);

router.post('/signup', userController.createUser);
router.post('/login', userController.login);

router.put('/access/:id', userController.changeAccessLevel);
router.put('/:id/favourite/add', userController.addFavourite);
router.put('/:id/favourite/delete', userController.removeFavourite);


module.exports = router;
