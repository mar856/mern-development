const express = require('express');
const router = express.Router();
const contributionController = require('../controllers/contribution.server.controller');

router.get('/by/:userId', contributionController.getContributionsByUser);

router.post('/update', contributionController.updateContribution);

router.get('/getContribution', contributionController.getContribution);

router.put('/review/:id', contributionController.reviewChange);

module.exports = router;