
const express = require('express');
const router = express.Router();
const favouriteController = require('../controllers/favourites.server.controller');

router.get('/get', favouriteController.getFavourites);
router.post('/add', favouriteController.addFavourite);
router.post('/remove', favouriteController.removeFavourite)

module.exports = router;

