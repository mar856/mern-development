const express = require('express');
const router = express.Router();
const dashboardController = require('../controllers/dashboard.server.controller');

router.get('/get-character', dashboardController.getData);
router.post('/accept-character', dashboardController.acceptCharacter);
// router.post('/decline-character', dashboardController.declineCharacter);
router.post('/archive-character', dashboardController.archiveCharacter);
router.post('/restore-character', dashboardController.restoreCharacter);

module.exports = router;
