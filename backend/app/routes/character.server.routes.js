const express = require('express');
const router = express.Router();
const characterController = require('../controllers/character.server.controller');

router.get('/get-characters', characterController.getCharacters);
router.get('/get/:id', characterController.getCharacterInfo);
router.put('/edit/:id', characterController.editCharacter);
router.post('/add', characterController.addCharacter)

module.exports = router;