
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const favouritesSchema = new Schema({
    "_id": mongoose.Types.ObjectId,
    "characters": {String},
});

const Favourite = mongoose.model("Favourite", favouritesSchema, "favourites");
module.exports = {
    Favourite,
    favouritesSchema
};