const mongoose = require('mongoose');
const { characterSchema } = require('./character');
const Schema = mongoose.Schema;

const APPROVED_STATUS = "approved";
const PENDING_STATUS = "pending";
const REJECT_STATUS = "rejected";
const ARCHIVE_STATUS = "archived";
const RESTORE_STATUS = "restored";

const ADD_CHARACTER = "AddCharacter";
const EDIT_CHARACTER = "EditCharacter";
const DELETE_CHARACTER = "DeleteCharacter";

const contributionSchema = new Schema({
    status: { type: String, enum: [APPROVED_STATUS, PENDING_STATUS, REJECT_STATUS, ARCHIVE_STATUS, RESTORE_STATUS] },
    user_id: String,
    reviewed_by: String,
    action: { type: String, enum: [ADD_CHARACTER, EDIT_CHARACTER, DELETE_CHARACTER] },
    date: { type: Date, default: Date.now },
    data: {
        old: characterSchema,
        new: characterSchema
    }
});

const Contribution = mongoose.model('Contribution', contributionSchema, "contributions");
module.exports = {
    Contribution,
    APPROVED_STATUS,
    PENDING_STATUS,
    REJECT_STATUS,
    ARCHIVE_STATUS,
    ADD_CHARACTER,
    EDIT_CHARACTER,
    DELETE_CHARACTER,
};
