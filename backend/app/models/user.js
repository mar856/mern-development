const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String,
    // isAdmin: Boolean,
    //favourites: [mongoose.Schema.Types.ObjectId] // array of character Id
});

const User = mongoose.model("User", userSchema, "users");
module.exports = User;