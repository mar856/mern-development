const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const characterSchema = new Schema({
    // _id: mongoose.Types.ObjectId, 
    active: Boolean,
    name: String,
    subtitle: String,
    description: String,
    image_url: String,
    strength: Number,
    speed: Number,
    skill: Number,
    fear_factor: Number,
    power: Number,
    intelligence: Number,
    wealth: Number
});

const Character = mongoose.model("Character", characterSchema, "characters");
module.exports = {
    Character,
    characterSchema
};