const mongoose = require('mongoose');
const { Admin } = require('../models/adminlist');
const User = require('../models/user');

const changeAdmin = async(req,res) => {
    const adminId = req.body.adminId;
    try {
        // Create a new document using the request body
        const existingAdmin = await Admin.findById(adminId);
        if (existingAdmin) {
            await Admin.findOneAndDelete({_id: adminId });
        }

        else{
            const newAdmin = await Admin({_id: adminId });
            await newAdmin.save();
            res.status(201).json(newAdmin);
        }
      } catch (error) {
        // Handle errors
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
      }

}


const isAdmin = async (req, res) => {
    const { id } = req.body;

    try {
        const existingAdmin = await Admin.findById(id);

        if (!existingAdmin) {
            return res.status(403).json({ message: 'Admin not found', status: false  });
        }

        return res.status(200).json({ message: 'Admin found', status: true });
    } catch (error) {
        console.error('Error fetching admin:', error);
        return res.status(500).json({ message: 'Internal server error' });
    }
}

const getAllAdmin = async (req, res) => {
    try {
        const admins = await Admin.find();

        if (!admins || admins.length === 0) {
            return res.status(403).json({ message: 'Admin not found', status: false  });
        }

        return res.status(200).json({ message: 'Success', data: admins });
    } catch (error) {
        console.error('Error fetching admin:', error);
        return res.status(500).json({ message: 'Internal server error' });
    }
}

const promoteUser = async (req, res) => {
    const session = await mongoose.startSession();
    try {
        const { userId, createdBy } = req.body;

        session.startTransaction();

        if (userId === createdBy)
            throw new Error('User unable to change their own role');

        const envokedBy = await Admin.findById(createdBy);
        const targetUser = await User.findById(userId).select(['firstName', 'lastName']);
        if (!envokedBy)
            throw new Error('User is not an admin');

        const promote = await Admin({_id: targetUser._id });
            await promote.save();

        if (!promote) {
            await session.abortTransaction();
            throw new Error("Failed to update user role");
            // return res.status(500).json({ message: "Failed to update user role" });
        }

        await session.commitTransaction();
        return res.status(200).json({ message: "Successfuly promote user" })

    } catch (error) {
        await session.abortTransaction();
        return res.status(500).json({ message: "Error promote user data", error: error, stack: error.stack });
    }
}
const demoteUser = async (req, res) => {
    const session = await mongoose.startSession();
    try {
        const { userId, createdBy } = req.body;

        session.startTransaction();

        if (userId === createdBy)
            throw new Error('User unable to change their own role');

        const envokedBy = await Admin.findById(createdBy);
        const targetUser = await User.findById(userId).select(['firstName', 'lastName']);
        if (!envokedBy)
            throw new Error('User is not an admin')

        const demote = await Admin.findByIdAndDelete(targetUser._id );

        if (!demote) {
            await session.abortTransaction();
            throw new Error("Failed to update user role");
        }

        await session.commitTransaction();
        return res.status(200).json({ message: "Successfuly demote user" })

    } catch (error) {
        await session.abortTransaction();
        return res.status(500).json({ message: "Error demote user data", error: error });
    }
}

module.exports = {
    changeAdmin,
    isAdmin,
    getAllAdmin,
    promoteUser,
    demoteUser
}