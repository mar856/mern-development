const UserModel = require('../models/user');
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const { History, APPROVED_STATUS } = require('../models/contribution');

const createUser = async (req, res) => {
    const { firstName, lastName, email, password } = req.body;

    try {
        const existingUser = await UserModel.findOne({ email });
        if (existingUser) {
            return res.status(409).json({ message: 'User already exists' });
        }
        let newUser = new UserModel({
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: bcrypt.hashSync(password, bcrypt.genSaltSync()),
        });
        await newUser.save();
        console.log("new user: ", newUser);
        console.log("password: ", password);

        const token = jwt.sign(
            { userId: newUser._id, email: email },
            'secret_key'
        );

        res.status(200).json({
            message: `User ${email} added successfully.`,
            token: token,
            userId: newUser._id,
        });
    } catch (error) {
        res.status(500).json({ message: 'Error creating user data', error: error });
    }
}

const getUser = async (req, res) => {
    try {
        const userId = req.params.id;
        const user = await UserModel.findById(userId).select('-password');
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }
        res.status(200).json(user);
    } catch (error) {
        res.status(500).json({ message: 'Error fetching user data', error: error });
    }
}



const login = async (req, res) => {
    const { email, password } = req.body;

    try {
        const user = await UserModel.findOne({ email });
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        // Check the password
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
            console.log(password, user.password);
            console.log(bcrypt.hashSync(password, bcrypt.genSaltSync()));
            return res.status(400).json({ message: 'Invalid credentials' });
        }

        const token = jwt.sign(
            { userId: user._id, email: email },
            'secret_key'
        );

        console.log("Token:", token);
        res.status(200).json({ message: 'Login successful', token: token, userId: user._id });
    } catch (error) {
        res.status(500).json({ message: 'Login failed', error: error.message || 'Unknown error' });
    }
}

const changeAccessLevel = async (req, res) => {
    try {
        const userId = req.params.id;
        const { adminId, isNowAdmin } = req.body;

        if (userId === adminId) {
            res.status(400).json({ message: "Admin cannot modify themselves" });
            return;
        }

        const user = await UserModel.findById(userId);
        if (!user) {
            res.status(404).json({ message: `User ${userId} not found` });
            return;
        }

        const username = user.firstName + ' ' + user.lastName;
        if (user.isAdmin && isNowAdmin) {
            res.status(400).json({ message: `${username} is already an admin` });
        } else if (!user.isAdmin && !isNowAdmin) {
            res.status(400).json({ message: `${username} is already a regular user` });
        } else {
            user.isAdmin = isNowAdmin;
            await user.save();
            res.status(200).json({ message: `${username}'s access level has been changed by ${adminId}` });
        }
    } catch (error) {
        res.status(500).json({ message: "Error updating user's access level", error: error });
    }
}

const addFavourite = async (req, res) => {
    try {
        const userId = req.params.id;
        const { characterId } = req.body;

        const user = await UserModel.findById(userId);
        if (!user) {
            return res.status(404).json({ message: `User ${userId} not found` });
        }
        if (user.favourites.includes(characterId)) {
            return res.status(409).json({ message: `${characterId} is already in the favourite list` });
        }
        user.favourites.push(characterId);
        await user.save();
        res.status(200).json({ message: `${characterId} added successfully.` });
    } catch (error) {
        res.status(500).json({ message: error.message || 'Unknown error' });
    }
}

const removeFavourite = async (req, res) => {
    try {
        const userId = req.params.id;
        const { characterId } = req.body;

        const user = await UserModel.findById(userId);
        if (!user) {
            return res.status(404).json({ message: `User ${userId} not found` });
        }

        const index = user.favourites.indexOf(characterId);
        if (index > -1) {
            user.favourites.splice(index, 1); // remove only one character
            await user.save();
            res.status(200).json({ message: `${characterId} deleted successfully.` });
        } else {
            return res.status(400).json({ message: `${characterId} does not exist in the favourite list` });
        }
    } catch (error) {
        res.status(500).json({ message: error.message || 'Unknown error' });
    }
}

const getAllUsers = async (req, res) => {
    try {
        const users = await UserModel.find().select(['-password', '-favourites']);
        if (!users) {
            return res.status(404).json({ message: 'users not found' });
        }
        res.status(200).json(users);
    } catch (error) {
        res.status(500).json({ message: 'Error fetching user data', error: error, stack: error.stack });
    }
}



module.exports = {
    getUser,
    createUser,
    login,
    changeAccessLevel,
    addFavourite,
    removeFavourite,
    getAllUsers,
}