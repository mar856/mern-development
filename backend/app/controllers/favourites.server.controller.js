const mongoose = require('mongoose');
const { Favourite } = require('../models/favourites');

const addFavourite = async(req,res) => {
    const { _id, favouriteCharacter } = req.body;
    // Check if the user exists
    try{
        const existingUser = await Favourite.findById(_id);
        if (existingUser) {
        // User exists, add favorite character to the characters array
        existingUser.characters.push(favouriteCharacter);
        await existingUser.save();
        res.status(200).json({ message: 'Favorite character added to existing user' });
        } else {
        // User doesn't exist, create a new user
        const newUser = new Favourite({ _id, characters: [favouriteCharacter] });
        await newUser.save();
        res.status(201).json({ message: 'New user created with favorite character' });
        }
    } catch (error) {
    // Handle errors
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }

}

const removeFavourite = async(req,res) => {
    // favouriteCharacter is the character id which should be a name.
    const { _id, favouriteCharacter } = req.body;
    try {
        const existingUser = await Favourite.findById(_id);
            if (existingUser) {

                const index = existingUser.characters.indexOf(favouriteCharacter);
        
                if (index !== -1) {
                // Remove the favorite character from the characters array
                existingUser.characters.splice(index, 1);
                await existingUser.save();
                res.status(200).json({ message: 'Favorite character removed' });
                } else {
                res.status(404).json({ message: 'Favorite character not found' });
                }
            } else {
                    res.status(404).json({ message: 'User not found' });
            }
        } catch (error) {
            // Handle errors
            console.error(error);
            res.status(500).json({ message: 'Internal server error' });
        }
}

const getFavourites = async (req, res) => {
    const { _id } = req.body;
    try {
      const existingUser = await Favourite.findById(_id);

      if (existingUser) {
        res.status(200).json(existingUser.characters);
      } else {
        res.status(404).json({ message: 'User not found' });
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Internal server error' });
    }
};


module.exports = {
    addFavourite,
    removeFavourite,
    getFavourites
}