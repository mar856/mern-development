const {Contribution} = require('../models/contribution');
const {Character} = require('../models/character');
const {APPROVED_STATUS, REJECT_STATUS} = require("../models/contribution");

const getContributionsByUser = async (req, res) => {
    try {
        const userId = req.params.userId;
        const contributions = await Contribution.find({ contributor: userId, status: APPROVED_STATUS });
        if (!contributions) {
            res.status(200).json([]);
        }
        res.status(200).json(contributions);
    } catch (error) {
        res.status(500).json({ message: 'Error getting contributions', error: error.message });
    }
}

const updateContribution = async (req, res) => {
    try {
        const userId = req.params.userId;
        const { status, user_id, reviewed_by, action, date, data } = req.body;

        // Create or update the contribution Contribution
        let contributionSample = new Contribution({
            status,
            user_id,
            reviewed_by,
            action,
            date,
            data
        });
        
        console.log(contributionSample)
        // Save the Contribution to the database
        await contributionSample.save();

        res.status(200).json({ message: 'Contribution updated successfully'});
    } catch (error) {
        res.status(500).json({ message: 'Error updating contribution', error: error.message });
    }
}

const getContribution = async (req, res) => {
    try {
        const  contributions = await Contribution.find().skip(skip).limit(limit);
        if (! contributions ||  contributions.length === 0) {
            return res.status(404).json({ message: "No  contributions found" });
        }
        return res.status(200).json(contributions);
        
        // const page = parseInt(req.params.page) || 1;
        // const limit = parseInt(req.params.limit) || 10;

        // const skip = (page - 1) * limit;

        // const  contributions = await Contribution.find().skip(skip).limit(limit);
        // const totalData = await Contribution.find().skip(skip).limit(limit).countDocuments();
        // const totalCount = await Contribution.countDocuments();
        // const totalPages = Math.ceil(totalCount / limit);
        // // const  contributions = await Character.find({ isActive: true });
        // if (! contributions ||  contributions.length === 0) {
        //     return res.status(404).json({ message: "No  contributions found" });
        // }
        // return res.status(200).json({
        //     page,
        //     limit,
        //     skip,
        //      contributions,
        //     totalCount,
        //     totalPages,
        //     totalData
        // });
    } catch (error) {
        return res.status(500).json({ message: error, stack: error.stack });
    }
}

const reviewChange = async (req, res) => {
    try {
        const contributionId = req.params.id;
        const { decision, adminId } = req.body;

        const Contribution = await Contribution.findById(contributionId);
        if (!Contribution) {
            return res.status(404).send('Contribution record not found.');
        }

        if (decision === APPROVED_STATUS) {
            Contribution.status = APPROVED_STATUS;
            Contribution.approvedBy = adminId;
            // update character
            const updatedCharacterData = Contribution.version.new;
            await Character.findByIdAndUpdate(Contribution.version.old._id, updatedCharacterData, null); // option field set to null
        } else if (decision === REJECT_STATUS) {
            Contribution.status = REJECT_STATUS;
        } else {
            return res.status(400).send('Invalid decision.');
        }

        await Contribution.save();
        res.status(200).send(`Contribution ${contributionId} has been ${decision} by ${adminId}`);
    } catch (error) {
        console.error('Error processing the review:', error);
        res.status(500).send('Server error while processing the request.');
    }
};

module.exports = {
    getContributionsByUser,
    getContribution,
    updateContribution,
    reviewChange
}