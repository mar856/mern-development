const mongoose = require('mongoose');
const { Character } = require('../models/character');
const { History, APPROVED_STATUS, PENDING_STATUS, REJECT_STATUS, ARCHIVE_STATUS, RESTORE_STATUS } = require('../models/contribution');

const getData = async (req, res) => {
    try {
        const characters = await Character.find({active:true});
        // const characters = await Character.find({ isActive: true });
        if (!characters || characters.length === 0) {
            return res.status(404).json({ message: "No characters found" });
        }
        return res.status(200).json(characters);
    } catch (error) {
        return res.status(500).json({ message: error, stack: error.stack });
    }
}

const acceptCharacter = async (req, res) => {
    const { id, userid } = req.body;
    if (id === undefined || userid === undefined)
        return res.status(500).json({ message: "Data empty" });
    const session = await mongoose.startSession();
    try {
        session.startTransaction();

        const update = await Character.findByIdAndUpdate(
            id,
            { isActive: true }
        );
        if (!update) {
            await session.abortTransaction();
            return res.status(500).json({ message: "Failed to update character status" });
        }

        let history = new History({
            status: APPROVED_STATUS,
            createdBy: userid,
            actionType: 'has accepted character',
        });
        await history.save();
        if (!history) {
            await session.abortTransaction();
            return res.status(500).json({ message: "Failed to store log" });
        }

        await session.commitTransaction();
        return res.status(200).json({ message: "Successfully accepted the character" });

    } catch (error) {
        session.abortTransaction();
        return res.status(500).json({ message: "error " + error, stack: error.stack });
    }
}
const rejectCharacter = async (req, res) => {
    const { id, userid } = req.body;
    if (id === undefined || userid === undefined)
        return res.status(500).json({ message: "Data empty" });
    const session = await mongoose.startSession();
    try {
        session.startTransaction();

        const update = await Character.findByIdAndUpdate(
            id,
            { isActive: true }
        );
        if (!update) {
            await session.abortTransaction();
            return res.status(500).json({ message: "Failed to reject character" });
        }

        let history = new History({
            status: REJECT_STATUS,
            createdBy: userid,
            actionType: 'has reject character',
        });
        await history.save();
        if (!history) {
            await session.abortTransaction();
            return res.status(500).json({ message: "Failed to store log" });
        }

        await session.commitTransaction();
        return res.status(200).json({ message: "Successfully reject the character" });

    } catch (error) {
        session.abortTransaction();
        return res.status(500).json({ message: "error " + error, stack: error.stack });
    }
}
const archiveCharacter = async (req, res) => {
    const { id, userid } = req.body;
    const session = await mongoose.startSession();
    try {
        session.startTransaction();

        const update = await Character.findByIdAndUpdate(
            id,
            { isActive: false }
        );
        if (!update) {
            await session.abortTransaction();
            return res.status(500).json({ message: "Failed to archive character status" });
        }

        let history = new History({
            status: ARCHIVE_STATUS,
            createdBy: userid,
            actionType: 'has archive character',
        });
        await history.save();
        if (!history) {
            await session.abortTransaction();
            return res.status(500).json({ message: "Failed to store log" });
        }

        await session.commitTransaction();
        return res.status(200).json({ message: "Successfully archive the character" });

    } catch (error) {
        session.abortTransaction();
        return res.status(500).json({ message: "error " + error, stack: error.stack });
    }
}
const restoreCharacter = async (req, res) => {
    const { id, userid } = req.body;
    const session = await mongoose.startSession();
    try {
        session.startTransaction();

        const update = await Character.findByIdAndUpdate(
            id,
            { isActive: true }
        );
        if (!update) {
            await session.abortTransaction();
            return res.status(500).json({ message: "Failed to restore character status" });
        }

        let history = new History({
            status: RESTORE_STATUS,
            createdBy: userid,
            actionType: 'has restore character',
        });
        await history.save();
        if (!history) {
            await session.abortTransaction();
            return res.status(500).json({ message: "Failed to store log" });
        }

        await session.commitTransaction();
        return res.status(200).json({ message: "Successfully restore the character" });

    } catch (error) {
        session.abortTransaction();
        return res.status(500).json({ message: "error " + error, stack: error.stack });
    }
}

module.exports = {
    getData,
    acceptCharacter,
    rejectCharacter,
    archiveCharacter,
    restoreCharacter
}