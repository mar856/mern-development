const mongoose = require('mongoose');
const { Character } = require('../models/character');
const {History} = require("../models/contribution");
const {PENDING_STATUS} = require("../models/contribution");

const getCharacters = async (req, res) => {
    try {
        const characters = await Character.find();
        // const characters = await Character.find({ isActive: true });
        if (!characters || characters.length === 0) {
            return res.status(404).json({ message: "No characters found" });
        }
        return res.status(200).json(characters);
    } catch (error) {
        return res.status(500).json({ message: error, stack: error.stack });
    }
}
const getCharacterInfo = async (req, res) => {
    try {
        const characterId = req.params.id;
        const character = await Character.findById(characterId);
        // const character = await Character.find({ isActive: true });
        if (!character || character.length === 0) {
            return res.status(404).json({ message: "No character found" });
        }
        return res.status(200).json(character);
    } catch (error) {
        return res.status(500).json({ message: error, stack: error.stack });
    }
}

const addCharacter = async(req,res)=>{
    const { isActive, name, subtitle, description, strength, speed, skill, fear_factor, power, intelligence, wealth } = req.body;

    try {
        const existingCharacter =await Character.findOne({name});
        if (existingCharacter) { 
            return res.status(409).json({ message: 'Character already exists' });
        }
        let newCharacter = new Character({
            isActive: isActive,
            name: name,
            subtitle: subtitle,
            description: description,
            strength: strength,
            speed: speed,
            skill: skill,
            fear_factor: fear_factor,
            power: power,
            intelligence: intelligence,
            wealth: wealth
        });
        console.log(newCharacter)
        await newCharacter.save();
        res.status(200).json(`Character ${name} added successfully.`);
    } catch (error) {
        console.error('Error adding character:', error);
        res.status(500).json({ message: 'Internal server error' });
    }
    
}


const editCharacter = async (req, res) => {
    try {
        const characterId = req.params.id;
        const { userId, updatedCharacter, actionType } = req.body;

        const currentCharacter = await Character.findById(characterId);
        if (!currentCharacter) {
            res.status(404).json({ message: "Character not found" });
            return;
        }
        const history = new History({
            status: PENDING_STATUS,
            contributor: userId,
            approvedBy: null,
            actionType: actionType,
            version: {
                old: currentCharacter,
                new: updatedCharacter
            }
        });
        await history.save();
        res.status(200).send(`Character ${currentCharacter.name} is edited and pending approval`);
    } catch (error) {
        console.error('Error editing character', error);
        res.status(500).send('Server error while processing the request.');
    }
}

module.exports = {
    addCharacter,
    getCharacters,
    editCharacter,
    getCharacterInfo,
}