
//Run File
const { clearCollections, addMockData } = require("./app/db-scripts.js");
const express = require('express');
const app = express();
const path = require('path');
const cors = require('cors');

const session = require('express-session')
const mongoose = require('mongoose');

const dashboardRouter = require('./app/routes/dashboard.server.routes');
const characterRouter = require('./app/routes/character.server.routes');
const userRoutes = require('./app/routes/user.server.routes');
const contributionRouter = require('./app/routes/contribution.server.routes.js');
const adminRouter = require('./app/routes/admin.server.router.js');

// Enable CORS for all route
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors()); // maybe removed later
app.set('views', path.join(__dirname, 'app/views'));

// routes
app.use('/dashboard', dashboardRouter);
app.use('/character', characterRouter);
app.use('/contribution', contributionRouter);
app.use('/user', userRoutes);
app.use('/admin', adminRouter);

// app.use(session({
//     secret: 'sshh', // can be replaced by random string
//     cookie: { maxAge: 600000 }
// }));

run();

async function run() {
    try {
        await mongoose.connect(`mongodb://localhost:27017/cartoonopia`);
        console.log('MongoDB connected');
        // await clearCollections();
        // await addMockData();
        app.listen(3000, () => console.log('app is listening on port 3000!'));
    } catch (err) {
        console.error('MongoDB Connection Error: ', err);
    }
}
