# DB Design

This file is for outlining the database structure and fields that have to be kept in common.

Engine: MongoDB

Port: 27017

DB name: Cartoonopia



### Schema

```json
Users {
    _id
    "firstName": String,
    "lastName": String,
    "email": String,
    "password": String, // this should be encrypted,
    "isAdmin": boolean,
    "favourites": Array character id
}

Characters {
    _id,
    "active": true,
    "name": String,
    "subtitle": String,
    "description": String,
    "image_url": String, // directory eg image/...
    "strength": Number,
    "speed": Number,
    "skill": Number,
    "fear_factor": Number,
    "power": Number,
    "intelligence": Number,
    "wealth": Number
}

Histories {
	_id
	"status": enum, (approved, rejected, pending)
	"contributor": String, // username
	"approvedBy": String, // username
	"actionType": String, // has created, has updated, has deleted, has approved
	"date": Date
	"version": {
		old: CharacterVersion,
		new: CharacterVersion
	}
}

// Below are not documents, but objects to be inserted in the documents above
CharacterVersion {
    "name": String,
    "subtitle": String,
    "description": String,
    "image_url": String, // directory eg image/...
    "strength": Number,
    "speed": Number,
    "skill": Number,
    "fear_factor": Number,
    "power": Number,
    "intelligence": Number,
    "wealth": Number
}
```

