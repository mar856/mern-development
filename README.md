# MERN DEVELOPMENT

This are group assignment from universities, this code are not the final version that we submitted, however, I plan to further develop and fix the bugs. Some of it's code are not mine alone, other team members are listed on the authors section.

## Project Description

This project is focused on comparing character attributes. It includes a feature where logged-in users can create new characters or edit existing ones, with all actions requiring admin approval. New users can create an account to join the system.

Admins have the authority to promote or demote other users from the admin role. They can also view the activity history of all users to monitor their actions. Additionally, admins can deactivate or activate characters. When a character is deactivated, it is hidden from regular users.

### Prerequisites

What things you need to install the software and how to install them

- MongoDB
- NPM (Version 10.2.4 or later)
- Node JS (Version 20.11.0 or later)
- Prefered Web Browser

### Installing

A step by step series of examples that tell you how to get a development env running

1. Clone / download whole project
2. Go to project file and open terminal / command prompt
3. Run `npm install` command on terminal / command prompt
4. Then `npm run install-all`
5. Go to `/frontend` folder, using your prefered text editor, copy and paste `.env.example` and name it `.env`. Ensure vite server URL are same port number as the backend. By default it's port 3000, `http://localhost:3000`.

   !["Check server.js file and find the number inside app.listen function"](https://gitlab.com/mar856/mern-development/-/blob/master/Screenshots/backend_app_listen.png?raw=true "Check port number from the code")

   Or alternatively, check the terminal when initiate the project.

   !["Check terminal for backend port number"](https://gitlab.com/mar856/mern-development/-/blob/master/Screenshots/terminal_app_listen.png?raw=true "Check port number from terminal")

6. Then `npm run dev`

If you want to run frontend and backend separately, follow bellow instruction.

# Initiate frontend and backend separately

## Frontend

Go to `/frontend` folder, and run `npm run dev` on terminal / command prompt. Then open prefered web browser, and type `localhost:5173`.

## Backend

Locally, create a database named localhost:27017/cartoonopia on mongoDB, then create 3 collections "users", "characters", "contributions", "adminList", and "favourites"

in `/backend` folder, run `node server.js` (first go to server.js and uncomment the line `addMockData()`)

if succesful, check if the local database now has the mock data. If yes, comment out `addMockData()` in `server.js`

## Built With

- [MongoDB](https://www.mongodb.com) - Database
- [ExpressJS](https://www.expressjs.com) - Backend
- [ReactJS](https://www.react.dev) - Frontend library
- [NodeJS](https://www.nodejs.org) - JavaScript runtime environment
- [Vite.js](https://www.vitejs.dev) - Frontend Hot Module Replacement (HMR)
- [Axios](https://axios-http.com) - Axios is a simple promise based HTTP client for the browser and node.js. Axios provides a simple to use library in a small package with a very extensible interface.
- [CORS](https://www.npmjs.com/package/cors) - stuffs
- [Express Session](https://github.com/expressjs/session) - stuffs
- [JSON Web Tokens](https://jwt.io/) - JSON Web Tokens is a website that allows you to manipulate JSON Web Tokens, an open, industry standard method for securely representing claims between two parties.
- [Concurrently](https://github.com/open-cli-tools/concurrently) - Run multiple commands concurrently
- [npm-run-all](https://github.com/mysticatea/npm-run-all) - A CLI tool to run multiple npm-scripts in parallel or sequential
- [bcrypt](https://github.com/kelektiv/node.bcrypt.js) - stuffs
- [ejs](https://ejs.co/) - stuffs
- [Mongoose](https://mongoosejs.com/docs/) - stuffs
- [Nodemon](https://nodemon.io/) - nodemon is a tool that helps develop Node.js based applications by automatically restarting the node application when file changes in the directory are detected.
- [Fontawesome](https://fontawesome.com/) - Icons
- [Bootstrap](https://getbootstrap.com/) - Bootstrap is a powerful, feature-packed frontend toolkit
- [React Bootstrap](https://react-bootstrap.github.io/) - React-Bootstrap replaces the Bootstrap JavaScript. Each component has been built from scratch as a true React component, without unneeded dependencies like jQuery.
- [React Icons](https://react-icons.github.io/react-icons/) - Provides icons that Fontawesome otherwise needs to pay
- [React Router DOM](https://reactrouter.com/en/main) - React Router enables "client side routing".
- [React Table Netlify](https://react-table-v7-docs.netlify.app/) - React Table is a collection of hooks for building powerful tables and datagrid experiences.
- [Vite Plugin HTML ENV](https://github.com/lxs24sxl/vite-plugin-html-env) - A Vite Plugin for rewriting html to display env variables in html

## Authors

- **Aishath Risla Umar** - _auma7001_
- **Kevin Liang** - _klia0633_
- **Martin Segoh** - _mseg3511_
- **Shou Miyamoto** - _smiy0645_

## Assumptions

- You can't add a new character given there is an old character that has its existing name.

## License

This project is licensed under the MIT License

## Acknowledgments

- chatGPT
- Phind AI
